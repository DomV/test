from django.contrib import admin

from .models import Building, Client, UserExtended, Apartment, Loan, BillDetails, Payment, Compensation

admin.site.register(Building)
admin.site.register(Client)
admin.site.register(UserExtended)
admin.site.register(Apartment)
admin.site.register(Loan)
admin.site.register(BillDetails)
admin.site.register(Payment)
admin.site.register(Compensation)