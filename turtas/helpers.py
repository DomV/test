import calendar
import math
import uuid

from dateutil.relativedelta import relativedelta
from datetime import datetime
from decimal import Decimal

import turtas.models


def is_last_day_of_the_month(date):
        days_in_month = calendar.monthrange(date.year, date.month)[1]
        return date.day == days_in_month


def calculate_monthdelta(date1, date2, including=True):
    imaginary_day_2 = 31 if is_last_day_of_the_month(date2) else date2.day
    monthdelta = (
        (date2.month - date1.month) +
        (date2.year - date1.year) * 12
    )
    if not including:
        monthdelta += -1 if date1.day > imaginary_day_2 else 0
    return monthdelta


def create_repayment_schedule(loan, document=False):
    paid = False
    start = loan.valid_from + relativedelta(loan.valid_from, months=+1)
    start2 = loan.valid_from
    end = loan.valid_to
    day_of_month = 20
    day_of_month2 = calendar.monthrange(start2.year, start2.month)[1]
    dates_document = [start2.replace(day=day_of_month2)]
    dates = [start.replace(day=day_of_month)]
    loan_repayment = loan.monthly_payment(loan_sum=loan.loan_sum, number_of_periods=loan.number_of_periods)
    credit_total = loan.loan_sum
    number_of_periods = loan.number_of_periods
    credit_balance = [loan.loan_sum]
    interest = [round(credit_balance[0] * (loan.interest_rate / 100) / loan.days_interest_is_payable * (day_of_month2 + 1 - start2.day), 2)]
    sum_to_pay = [interest[0] + loan_repayment]
    loan_repayments = [loan_repayment]
    early_payments = []
    payments, payments2 = 0, 0
    if loan.apartment:
        if not loan.accrued_interest:
            payments = paid_credit_amount_of_apartment(loan.apartment, start2, end)
            payments2 = paid_interest_amount_of_apartment(loan.apartment, start2, end)
            early_payments = turtas.models.Payment.objects.filter(apartment=loan.apartment, payment_type=2)
    else:
        if not loan.accrued_interest:
            payments = paid_credit_amount_of_apartment(loan.building, start2, end)
            payments2 = paid_interest_amount_of_apartment(loan.building, start2, end)
            early_payments = turtas.models.Payment.objects.filter(building=loan.building, payment_type=2)
    add_repayments = 0
    if not loan.accrued_interest:
        loans = turtas.models.Loan.objects.filter(apartment=loan.apartment)
        for loan2 in loans:
            if loan2 != loan and loan2.accrued_interest:
                add_repayments = loan2.monthly_payment_original
    first_payment = loan_repayments[0] + add_repayments if payments - loan_repayments[0] - add_repayments > 0 else payments
    payments = max(payments - loan_repayments[0], 0)

    for payment in early_payments:
        if payment.payment_date.year <= dates_document[-1].year and payment.payment_date.month <= dates_document[-1].month:
            first_payment += payment.payment_sum
    loan_repayments_done = [first_payment]

    first_payment2 = interest[0] if payments2 - interest[0] > 0 else payments2
    payments2 = max(payments2 - interest[0], 0)

    interests_done = [first_payment2]
    total_done = [first_payment + first_payment2]

    now = datetime.now().date()
    is_due = [now > start]
    # i = 1
    periods = iter(range(loan.number_of_periods))
    if loan.apartment:
        compensated_payments = turtas.models.Payment.objects.filter(apartment=loan.apartment.pk, payment_type=5)
    for i in periods:
        if credit_balance[-1] < loan_repayments_done[-1]:
            paid = True
        new_date = dates[-1] + relativedelta(dates[-1], months=+1)
        start2 = start2 + relativedelta(start2, months=+1)
        if new_date.month == end.month and new_date.year == end.year:
            day_of_month = calendar.monthrange(end.year, end.month)[1]
            day_of_month2 = calendar.monthrange(start2.year, start2.month)[1]
            dates_document.append(start2.replace(day=day_of_month2))
            dates.append(end)
        else:
            day_of_month = 20
            day_of_month2 = calendar.monthrange(start2.year, start2.month)[1]
            dates_document.append(start2.replace(day=day_of_month2))
            dates.append(new_date.replace(day=day_of_month))

        repayment_done = 0

        # print(loan_repayments_done[-1], credit_balance[-1])
        new_credit_balance = credit_balance[-1] - loan_repayments[-1]
        for payment in early_payments:
            if len(dates) > 1:
                if payment.payment_date.year == dates_document[-2].year and payment.payment_date.month == dates_document[-2].month:
                    # new_credit_balance -= payment.payment_sum
                    credit_total -= payment.payment_sum
                    new_credit_balance -= payment.payment_sum
                    loan_repayment = loan.monthly_payment(loan_sum=new_credit_balance, number_of_periods=number_of_periods - i - 1)
        if credit_balance[-1] < loan_repayment or paid:
            credit_balance.append(0)
            interest.append(0)
            sum_to_pay.append(0)
            loan_repayments.append(0)
            loan_repayments_done.append(0)
            interests_done.append(0)
            total_done.append(0)
            is_due.append(now > new_date)
            # next(periods, None)
            continue
        credit_balance.append(new_credit_balance)
        days = 366 if calendar.isleap(start2.year) else 365
        interest.append(round((credit_balance[-1] * (loan.interest_rate / 100) / days * 19) + (credit_balance[-2] * (loan.interest_rate / 100) / days * (day_of_month2 - 19)), 2))
        if new_date.month == end.month and new_date.year == end.year:
            loan_repayments.append(credit_balance[-1])
            sum_to_pay.append(interest[-1] + loan_repayments[-1])
            interest[-1] = round((credit_balance[-1] * (loan.interest_rate / 100) / days * 19) + (credit_balance[-2] * (loan.interest_rate / 100) / days * (day_of_month2 - 19 + day_of_month - 1)), 2)
        else:
            sum_to_pay.append(interest[-1] + loan_repayment)
            loan_repayments.append(loan_repayment)
        interest_done = interest[-1] if payments2 - interest[-1] > 0 else payments2
        repayment_done = loan_repayments[-1] + add_repayments if payments - loan_repayments[-1] - add_repayments > 0 else payments
        if loan.apartment:
            for payment in compensated_payments:
                if dates[-1] > payment.payment_date > dates[-1]:
                    interest_done = interest[-1]
                    repayment_done = loan_repayments[-1]
        if datetime.now().date() < new_date:
            interest_done = 0
            repayment_done = 0
        for payment in early_payments:
            if payment.payment_date.year == dates_document[-1].year and payment.payment_date.month == dates_document[-1].month:
                repayment_done += payment.payment_sum
        interests_done.append(interest_done)
        loan_repayments_done.append(repayment_done)

        total_done.append(repayment_done + interest_done)
        is_due.append(now > new_date)
        payments = max(payments - loan_repayments[-1] - add_repayments, 0)
        payments2 = max(payments2 - interest[-1], 0)
    if document:
        dates = dates_document
    return [list(item) for item in zip(dates, credit_balance, interest, sum_to_pay, loan_repayments, loan_repayments_done, interests_done, total_done, is_due)]


def calculate_payment_between_dates(schedule, start, end):
    for dates, credit_balance, interest, sum_to_pay in schedule[0]:
        if start <= dates <= end:
            return [interest, schedule[1], sum_to_pay]


def credit_balance_of_apartment(apartment, date):
    payments = turtas.models.Payment.objects.filter(payment_type='0', apartment=apartment.pk, payment_date__lt=date)
    early_payments = turtas.models.Payment.objects.filter(payment_type='2', apartment=apartment.pk, payment_date__lt=date)
    loan_sum = apartment.get_loan_sum()
    for payment in payments:
        loan_sum -= payment.payment_sum
    for payment in early_payments:
        loan_sum -= payment.payment_sum
    return loan_sum


def payments_of_apartment(apartment, start, end):
    loans = turtas.models.Loan.objects.filter(apartment=apartment.pk)
    loan_to_repay = 0
    interest_to_pay = 0
    total_to_pay = 0
    for loan in loans:
        schedule = create_repayment_schedule(loan)
        for dates, credit_balance, interest, sum_to_pay, loan_to_pay, _, _, _, _ in schedule:
            if start <= dates <= end:
                loan_to_repay += loan_to_pay
                interest_to_pay += interest
                total_to_pay += sum_to_pay
    return [loan_to_repay, interest_to_pay, total_to_pay]


def credit_balance_of_loan(loan, date):
    if loan.apartment:
        payments = turtas.models.Payment.objects.filter(apartment=loan.apartment, payment_type='0', payment_date__lte=date)
        early_payments = turtas.models.Payment.objects.filter(apartment=loan.apartment, payment_type='2', payment_date__lte=date)
        loans = turtas.models.Loan.objects.filter(apartment=loan.apartment)
        loan_sum = loan.loan_sum
        if loan.accrued_interest:
            for apartment_loan in loans:
                if loan != apartment_loan and loan.accrued_interest != apartment_loan.accrued_interest:
                    second_loan = apartment_loan
                    early_payment_sum = 0
                    for payment in early_payments:
                        early_payment_sum += payment.payment_sum
                    if second_loan.outstanding_loan_balance(datetime.now()) == 0:
                        loan_sum -= second_loan.loan_sum - paid_loan_amount_of_apartment(second_loan.apartment, second_loan.valid_from, second_loan.valid_to)
                    for payment in payments:
                        if payment.payment_sum > second_loan.monthly_payment(loan_sum=loan.loan_sum, number_of_periods=loan.number_of_periods):
                            loan_sum -= payment.payment_sum - second_loan.monthly_payment(loan_sum=loan.loan_sum, number_of_periods=loan.number_of_periods)
        else:
            for payment in payments:
                # if payment.payment_sum >= loan.monthly_payment(loan_sum=loan.loan_sum, number_of_periods=loan.number_of_periods):
                #     loan_sum -= loan.monthly_payment(loan_sum=loan.loan_sum, number_of_periods=loan.number_of_periods)
                # else:
                loan_sum -= payment.payment_sum
            for payment in early_payments:
                loan_sum -= payment.payment_sum
    else:
        loan_sum = 0
    return loan_sum if loan_sum > 0 else 0


def paid_amount_of_apartment(apartment, start, end):
    payments = turtas.models.Payment.objects.filter(apartment=apartment.pk, payment_date__gte=start, payment_date__lte=end).exclude(payment_type=6)
    # compensations = turtas.models.Compensation.objects.filter(apartment=apartment.pk, date_from__gte=start, date_from__lte=end)
    paid = 0
    for payment in payments:
        if payment.payment_sum:
            paid += payment.payment_sum
    # for compensation in compensations:
    #     print(compensation)
    #     if compensation.compensated_sum:
    #         paid += compensation.compensated_sum
    return max(paid, 0)


def paid_other_amount_of_apartment(apartment, start, end):
    payments = turtas.models.Payment.objects.filter(apartment=apartment.pk, payment_date__gte=start, payment_date__lte=end, payment_type=2)
    paid = 0
    for payment in payments:
        if payment.payment_sum:
            paid += payment.payment_sum
    return max(paid, 0)


def paid_credit_amount_of_apartment(apartment, start, end):
    compensations = None
    if isinstance(apartment, turtas.models.Apartment):
        payments = turtas.models.Payment.objects.filter(apartment=apartment.pk, payment_date__gte=start, payment_date__lte=end, payment_type=0)
        compensations = turtas.models.Payment.objects.filter(apartment=apartment.pk, payment_date__gte=start, payment_date__lte=end, payment_type=5)
        monthly_payment = max([loan.monthly_payment_original for loan in apartment.loan_set.all()])
    else:
        payments = turtas.models.Payment.objects.filter(building=apartment.pk, payment_date__gte=start, payment_date__lte=end, payment_type=0)
    paid = 0
    for payment in payments:
        if payment.payment_sum:
            paid += payment.payment_sum
    if compensations:
        for payment in compensations:
            paid += monthly_payment if monthly_payment >= payment.payment_sum else payment.payment_sum
    return max(paid, 0)


def paid_interest_amount_of_apartment(apartment, start, end):
    compensations = None
    if isinstance(apartment, turtas.models.Apartment):
        payments = turtas.models.Payment.objects.filter(apartment=apartment.pk, payment_date__gte=start, payment_date__lte=end, payment_type=1)
        compensations = turtas.models.Payment.objects.filter(apartment=apartment.pk, payment_date__gte=start, payment_date__lte=end, payment_type=5)
        monthly_payment = max([loan.monthly_payment_original for loan in apartment.loan_set.all()])
    else:
        payments = turtas.models.Payment.objects.filter(building=apartment.pk, payment_date__gte=start, payment_date__lte=end, payment_type=1)
    paid = 0
    for payment in payments:
        if payment.payment_sum:
            paid += payment.payment_sum
    if compensations:
        for payment in compensations:
            paid += payment.payment_sum - monthly_payment
    return max(paid, 0)


def paid_loan_amount_of_apartment(apartment, start, end):
    payments = turtas.models.Payment.objects.filter(apartment=apartment.pk, payment_date__gte=start, payment_date__lte=end, payment_type__in=[0, 2])
    paid = 0
    for payment in payments:
        if payment.payment_sum:
            paid += payment.payment_sum
    return max(paid, 0)


def compensated_amount_of_apartment(apartment, start, end):
    payments = turtas.models.Payment.objects.filter(apartment=apartment.pk, payment_date__gte=start, payment_date__lte=end, payment_type=5)
    paid = 0
    for payment in payments:
        if payment.payment_sum:
            paid += payment.payment_sum
    return max(paid, 0)


def overpaid_amount_of_apartment(apartment, start, end):
    payments = turtas.models.Payment.objects.filter(apartment=apartment.pk, payment_date__gte=start, payment_date__lte=end, payment_type=6)
    paid = 0
    for payment in payments:
        if payment.payment_sum:
            paid += payment.payment_sum
    return max(paid, 0)


def paid_early_amount_of_apartment(apartment):
    if isinstance(apartment, turtas.models.Apartment):
        payments = turtas.models.Payment.objects.filter(apartment=apartment.pk, payment_type=7)
    else:
        payments = turtas.models.Payment.objects.filter(building=apartment.pk, payment_type=7)
    paid = 0
    for payment in payments:
        if payment.payment_sum:
            paid += payment.payment_sum
    return max(paid, 0)


def unpaid_interest_amount_of_apartment(apartment, to):
    paid, unpaid = 0, 0
    main_loan = apartment.loan_set.all()[0]
    for loan in apartment.loan_set.all():
        if not loan.accrued_interest:
            main_loan = loan
    schedule = create_repayment_schedule(main_loan)
    if to < schedule[0][0]:
        to = schedule[0][0]
    today = datetime.now().date()
    for dates, credit_balance, interest, sum_to_pay, loan_repayments, loan_repayments_done, interests_done, total_done, is_due in schedule:
        if dates <= to:
            unpaid += interest
            paid += interests_done
    return max(unpaid - paid, 0)


def unpaid_credit_amount_of_apartment(apartment, to):
    paid, unpaid = 0, 0
    main_loan = apartment.loan_set.all()[0]
    second_loan = apartment.loan_set.all()[0]
    for loan in apartment.loan_set.all():
        if not loan.accrued_interest:
            main_loan = loan
        else:
            second_loan = loan
    schedule = create_repayment_schedule(main_loan)
    schedule2 = create_repayment_schedule(second_loan)
    early_payments = turtas.models.Payment.objects.filter(apartment=apartment, payment_date__lt=to, payment_type=2)
    today = datetime.now().date()
    if to < schedule[0][0]:
        to = schedule[0][0]
    for dates, credit_balance, interest, sum_to_pay, loan_repayments, loan_repayments_done, interests_done, total_done, is_due in schedule:
        if dates <= to:
            date = dates + relativedelta(months=-1)
            for payment in early_payments:
                if payment.payment_date.month == date.month and payment.payment_date.year == date.year:
                    loan_repayments_done -= payment.payment_sum
            unpaid += loan_repayments
            paid += loan_repayments_done
    for dates, credit_balance, interest, sum_to_pay, loan_repayments, loan_repayments_done, interests_done, total_done, is_due in schedule2:
        if dates <= to:
            unpaid += loan_repayments
            paid += loan_repayments_done
    return max(unpaid - paid, 0)


def fine_list(building=None, apartment=None, to=None):
    if to is None:
        to = datetime.now().date()
    fines = []
    if building:
        apartments = turtas.models.Apartment.objects.filter(building=building)
    elif apartment:
        apartments = [apartment]
    else:
        apartments = turtas.models.Apartment.objects.all()
    for apartment in apartments:
        fine = {}
        last_payment = turtas.models.Payment.objects.filter(apartment=apartment.pk, payment_type__in=[0, 1, 3, 5])
        loans = apartment.loan_set.all()
        if sum([float(loan.outstanding_loan_balance(to)) if not loan.accrued_interest else 0 for loan in loans]) > 0 and apartment.overpaid <= 0:
            if len(last_payment) > 0:
                last_payment_next = (last_payment.latest('payment_date').payment_date + \
                                relativedelta(last_payment.latest('payment_date').payment_date, months=+1)).replace(day=1)
            else:
                last_payment_next = to
            if (len(last_payment) == 0 or calculate_monthdelta(last_payment_next,
                                                               to, False) > 0) and len(apartment.loan_set.all()) > 0:
                fine['apartment_no'] = apartment.code
                if len(loans) > 1:
                    fine['loan_code'] = loans[0].contract_no if loans[1].accrued_interest else loans[1].contract_no
                apartment_credit = 0
                apartment_credit_balance = 0
                for loan in loans:
                    apartment_credit += loan.loan_sum
                    apartment_credit_balance += round(float(loan.outstanding_loan_balance(to)), 2)

                fine['apartment_credit'] = apartment_credit
                fine['apartment_credit_balance'] = apartment_credit_balance
                fine['client'] = apartment.client
                if len(last_payment) > 0:
                    fine['last_payment_date'] = last_payment.latest('payment_date').payment_date
                    start = (last_payment.latest('payment_date').payment_date + relativedelta(months=2)).replace(day=1)
                    start_pay = last_payment.latest('payment_date').payment_date
                else:
                    fine['last_payment_date'] = '-'
                    start = (apartment.loan_set.all()[0].valid_from + relativedelta(months=1)).replace(day=1)
                    start_pay = apartment.loan_set.all()[0].valid_from
                days_late = (to - start).days
                if days_late > 0:
                    periods = math.ceil(days_late / 30) or 1
                    payments = payments_of_apartment(apartment, start_pay, to)
                    fine['credit_to_pay'] = payments[0]
                    fine['interest_to_pay'] = payments[1]
                    fine['credit_payment'] = round(payments[0] / periods, 2)
                    fine['interest_payment'] = round(payments[1] / periods, 2)
                    fine['days_late'] = days_late
                    fine['fine'] = round(payments[2] / Decimal(days_late / 30) * Decimal(0.0004) * days_late, 2)
                    fines.append(fine)
    return fines


def calculate_compensation_sums(apartment, date_from, date_to):
    date_from = date_from + relativedelta(date_from, months=1)
    date_to = date_to + relativedelta(date_to, months=1)
    schedules = []
    if len(apartment.loan_set.all()) > 0:
        complete_schedule = create_repayment_schedule(apartment.loan_set.all()[0])
        for loan in apartment.loan_set.all():
            schedules.append(create_repayment_schedule(loan))
        for i in range(len(schedules)):
            if i > 0:
                j = 0
                for dates, credit_balance, interest, sum_to_pay, loan_repayments, _, _, _, _ in schedules[i]:
                    if j < len(complete_schedule):
                        complete_schedule[j][1] += credit_balance
                        complete_schedule[j][2] += interest
                        complete_schedule[j][3] += sum_to_pay
                        complete_schedule[j][4] += loan_repayments
                        j += 1
        compensated_sum = 0
        compensated_credit = 0
        compensated_interest = 0
        credit_balance_on_day_of_compensation = 0
        for schedule in complete_schedule:
            if date_from <= schedule[0] <= date_to:
                compensated_sum += schedule[3]
                compensated_credit += schedule[4]
                compensated_interest += schedule[2]
                credit_balance_on_day_of_compensation = schedule[1]
        return [compensated_sum, compensated_credit, compensated_interest, credit_balance_on_day_of_compensation]


def automatic_payments(apartment, payment_date, payment_sum, building, currency, added_by, name):
    fine = 0
    if apartment:
        fines = fine_list(apartment=apartment, to=payment_date)
        if len(fines) > 0:
            fine = fines[0]['fine']
            apartment.fine = fine
            apartment.save()
    fine_to_pay = apartment.current_fine()
    date = payment_date
    day_of_month = calendar.monthrange(date.year, date.month)[1]
    payments = unpaid_interest_amount_of_apartment(apartment, to=payment_date.replace(day=20) + relativedelta(months=1))
    unpaid_credit = unpaid_credit_amount_of_apartment(apartment, to=payment_date.replace(day=20) + relativedelta(months=1))
    # print(payments)
    # print(unpaid_credit)
    payment_sum = payment_sum
    fine_payment = 0
    left_sum = 0
    if payment_sum < 0:
        negative = True
        payment_sum = -payment_sum
        payments2 = payments_of_apartment(apartment, date.replace(day=1), date.replace(day=day_of_month))
        first_payment = payments2[1] if payments2[1] < payment_sum else payment_sum
        second_payment = payments2[0] if payment_sum - payments2[1] > payments2[0] else max(payment_sum - payments2[0], 0.00)
    else:
        negative = False
        first_payment = payments if payments < payment_sum else payment_sum
        second_payment = unpaid_credit if payment_sum - payments > unpaid_credit else max(payment_sum - payments, 0.00)
        left_sum = payment_sum - payments - unpaid_credit
        if left_sum > 0:
            fine_payment = left_sum if left_sum < fine_to_pay else fine_to_pay
            left_sum -= Decimal(fine_to_pay)
            apartment.overpaid += left_sum
            apartment.save()
    if negative:
        first_payment = -first_payment
        second_payment = -second_payment
    auto_id = uuid.uuid4()
    if not name:
        name = 'auto_' + str(date.month)
    if second_payment > 0:
        turtas.models.Payment(name=name + '_1', payment_date=payment_date,
                       apartment=apartment, building=building, payment_type=0,
                       payment_sum=second_payment, currency=currency,
                       added_by=added_by, auto_id=auto_id).save()
    if first_payment > 0:
        turtas.models.Payment(name=name + '_2', payment_date=payment_date,
                       apartment=apartment, building=building, payment_type=1,
                       payment_sum=first_payment, currency=currency,
                       added_by=added_by, auto_id=auto_id).save()
    if fine_payment > 0:
        turtas.models.Payment(name=name + '_3', payment_date=payment_date,
                       apartment=apartment, building=building, payment_type=3,
                       payment_sum=fine_payment, currency=currency,
                       added_by=added_by, auto_id=auto_id).save()
    if left_sum > 0:
        turtas.models.Payment(name=name + '_6', payment_date=payment_date,
                       apartment=apartment, building=building, payment_type=6,
                       payment_sum=left_sum, currency=currency,
                       added_by=added_by, auto_id=auto_id).save()