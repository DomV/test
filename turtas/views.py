import calendar
import uuid
from datetime import datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import redirect, render
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, DetailView, RedirectView, CreateView, UpdateView, DeleteView, TemplateView
from num2words import num2words

from weasyprint import HTML

from .filters import BuildingFilter, PaymentFilter, ApartmentFilter, LoanFilter, ClientFilter
from .helpers import (calculate_monthdelta, calculate_payment_between_dates, create_repayment_schedule,
                      credit_balance_of_apartment, paid_amount_of_apartment, payments_of_apartment, fine_list,
                      paid_other_amount_of_apartment, compensated_amount_of_apartment,
                      unpaid_interest_amount_of_apartment, unpaid_credit_amount_of_apartment,
                      calculate_compensation_sums, overpaid_amount_of_apartment, paid_credit_amount_of_apartment,
                      paid_interest_amount_of_apartment, paid_loan_amount_of_apartment, automatic_payments)
from .models import Building, Client, Apartment, Loan, Bill, Payment, BillDetails, Compensation, UserExtended

# Create your views here.


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)


class BuildingList(LoginRequiredMixin, ListView):
    template_name = 'building_list.html'

    def get_queryset(self, **kwargs):
        if self.request.user.is_superuser:
            if self.request.GET:
                self.request.session['buildings'] = self.request.GET
            saved_request = self.request.session.get('buildings', self.request.GET)
            self.request.GET = saved_request
            queryset = BuildingFilter(saved_request)
            paginator = Paginator(queryset, 25)
            page = self.request.GET.get('page')
            try:
                queryset = paginator.page(page)
            except PageNotAnInteger:
                queryset = paginator.page(1)
            except EmptyPage:
                queryset = paginator.page(paginator.num_pages)
        else:
            # queryset = Building.objects.filter(added_by=self.request.user.userextended)
            if self.request.GET:
                self.request.session['buildings'] = self.request.GET
            saved_request = self.request.session.get('buildings', self.request.GET)
            self.request.GET = saved_request
            queryset = BuildingFilter(saved_request, queryset=Building.objects.all()).qs
            paginator = Paginator(queryset, 25)
            page = self.request.GET.get('page')
            try:
                queryset = paginator.page(page)
            except PageNotAnInteger:
                queryset = paginator.page(1)
            except EmptyPage:
                queryset = paginator.page(paginator.num_pages)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(BuildingList, self).get_context_data(**kwargs)
        context['buildings'] = list(Building.objects.all().values_list('name', flat=True))
        return context


class BuildingDetail(LoginRequiredMixin, DetailView):
    model = Building
    template_name = 'building_detail.html'

    def get_context_data(self, **kwargs):
        context = super(BuildingDetail, self).get_context_data(**kwargs)
        building = self.get_object()
        context['buildings'] = Building.objects.all()
        context['apartments'] = Apartment.objects.all()
        context['clients'] = Client.objects.all()
        schedules = []
        if len(building.loan_set.all()) > 0:
            loans = list(building.loan_set.all())
            main_loan = None
            for loan in loans:
                if not loan.accrued_interest:
                    main_loan = loan
                    loans.remove(loan)
            if not main_loan:
                context['no_loan'] = True
                main_loan = loans[0]
            complete_schedule = create_repayment_schedule(main_loan)
            for loan in loans:
                schedules.append(create_repayment_schedule(loan))
            for i in range(len(schedules)):
                j = 0
                for dates, credit_balance, interest, sum_to_pay, loan_repayments, loan_repayments_done, interests_done, total_done, is_due in schedules[i]:
                    if len(complete_schedule) > j:
                        complete_schedule[j][1] += credit_balance
                        complete_schedule[j][2] += interest
                        complete_schedule[j][3] += sum_to_pay
                        complete_schedule[j][4] += loan_repayments
                        # complete_schedule[j][5] += loan_repayments_done
                        complete_schedule[j][6] += interests_done
                        complete_schedule[j][7] += total_done
                        complete_schedule[j][8] += is_due
                        j += 1
            loan_repayments_total = 0
            interest_total = 0
            sum_to_pay_total = 0
            for dates, credit_balance, interest, sum_to_pay, loan_repayments, loan_repayments_done, interests_done, total_done, is_due in complete_schedule:
                loan_repayments_total += loan_repayments
                interest_total += interest
                sum_to_pay_total += sum_to_pay
            context['schedule'] = complete_schedule
            context['loan_repayments_total'] = loan_repayments_total
            context['interest_total'] = interest_total
            context['sum_to_pay_total'] = sum_to_pay_total
        return context


class BuildingCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'new_building.html'
    model = Building
    success_url = '/buildings/'
    success_message = 'Pastatas sėkmingai įvestas!'
    fields = ['name', 'floor_count', 'apartment_count', 'address', 'area', 'city', 'description',
              'renovation_finished_at', 'district', 'post_code', 'country', 'extra_requisites', 'invoice_note']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BuildingCreate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        form.instance.added_by = self.request.user.userextended
        return super(BuildingCreate, self).form_valid(form)


class BuildingUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'update_building.html'
    model = Building
    success_url = '/buildings/'
    success_message = 'Pastatas sėkmingai atnaujintas!'
    fields = ['name', 'floor_count', 'apartment_count', 'address', 'area', 'city', 'description',
              'renovation_finished_at', 'district', 'post_code', 'country', 'extra_requisites', 'invoice_note']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BuildingUpdate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        form.instance.added_by = self.request.user.userextended
        return super(BuildingUpdate, self).form_valid(form)


class BuildingDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Building
    success_message = 'Pastatas sėkmingai ištrintas!'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BuildingDelete, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return self.request.GET['next'] if self.request.GET['next'] else '/buildings/'


class BuildingBalance(LoginRequiredMixin, DetailView):
    template_name = 'building_balance.html'
    model = Building

    def get_context_data(self, **kwargs):
        context = super(BuildingBalance, self).get_context_data(**kwargs)
        context['object'] = Building.objects.get(pk=self.kwargs['pk'])
        apartments = Apartment.objects.filter(building=self.kwargs['pk'])
        start = datetime.strptime(self.request.GET['start'], '%Y-%m-%d').date()
        end = datetime.strptime(self.request.GET['end'], '%Y-%m-%d').date()
        apartment_balance = []
        start_balance_total = 0
        end_balance_total = 0
        loan_to_repay_total = 0
        interest_to_pay_total = 0
        total_to_pay_total = 0
        paid_total = 0
        debt_total = 0
        fine_total = 0
        paid_other_total = 0
        compensated_total = 0
        for apartment in apartments:
            single_apartment = {}
            scheduled_payments = payments_of_apartment(apartment, start, end)
            total_to_pay = scheduled_payments[2]
            paid = paid_amount_of_apartment(apartment, start, end)
            paid_loan = paid_loan_amount_of_apartment(apartment, start, end)
            paid_interest = paid_interest_amount_of_apartment(apartment, start, end)
            paid_other = paid_other_amount_of_apartment(apartment, start, end)
            compensated = compensated_amount_of_apartment(apartment, start, end)
            overpaid = overpaid_amount_of_apartment(apartment, start, end)
            single_apartment['code'] = apartment.code
            single_apartment['apartment_no'] = apartment.apartment_no
            single_apartment['client'] = apartment.client
            single_apartment['start_balance'] = credit_balance_of_apartment(apartment, start)
            single_apartment['end_balance'] = credit_balance_of_apartment(apartment, end)
            single_apartment['loan_to_repay'] = scheduled_payments[0]
            single_apartment['interest_to_pay'] = scheduled_payments[1]
            single_apartment['total_to_pay'] = total_to_pay
            single_apartment['paid'] = paid
            single_apartment['debt'] = total_to_pay - paid + paid_other - overpaid
            single_apartment['fine'] = apartment.ongoing_fine()
            single_apartment['other'] = paid_other
            single_apartment['compensated'] = compensated
            single_apartment['paid_loan'] = paid_loan
            single_apartment['paid_interest'] = paid_interest
            apartment_balance.append(single_apartment)
            start_balance_total += credit_balance_of_apartment(apartment, start)
            end_balance_total += credit_balance_of_apartment(apartment, end)
            loan_to_repay_total += scheduled_payments[0]
            interest_to_pay_total += scheduled_payments[1]
            total_to_pay_total += total_to_pay
            paid_total += paid
            debt_total += total_to_pay - paid
            fine_total += round(float(apartment.ongoing_fine()), 2)
            paid_other_total += paid_other
            compensated_total += compensated

        context['apartments'] = apartment_balance
        context['start_balance_total'] = start_balance_total
        context['end_balance_total'] = end_balance_total
        context['loan_to_repay_total'] = loan_to_repay_total
        context['interest_to_pay_total'] = interest_to_pay_total
        context['total_to_pay_total'] = total_to_pay_total
        context['paid_total'] = paid_total
        context['debt_total'] = debt_total
        context['start'] = start
        context['end'] = end
        context['fine_total'] = round(fine_total, 2)
        context['paid_other_total'] = paid_other_total
        context['compensated_total'] = compensated_total
        return context

    def get(self, request, *args, **kwargs):
        self.object = None
        htmlstring = render_to_string(self.template_name, self.get_context_data(), context_instance=RequestContext(request))
        html = HTML(string=htmlstring, base_url=request.build_absolute_uri())
        main_doc = html.render()
        pdf = main_doc.write_pdf()
        return HttpResponse(pdf, content_type='application/pdf')


class ClientList(LoginRequiredMixin, ListView):
    template_name = 'client_list.html'

    def get_queryset(self, **kwargs):
        if self.request.user.is_superuser:
            if self.request.GET:
                self.request.session['clients'] = self.request.GET
            saved_request = self.request.session.get('clients', self.request.GET)
            self.request.GET = saved_request
            queryset = ClientFilter(saved_request, queryset=Client.objects.all()).qs
            paginator = Paginator(queryset, 25)
            page = self.request.GET.get('page')
            try:
                queryset = paginator.page(page)
            except PageNotAnInteger:
                queryset = paginator.page(1)
            except EmptyPage:
                queryset = paginator.page(paginator.num_pages)
        else:
            # queryset = Building.objects.filter(added_by=self.request.user.userextended)
            if self.request.GET:
                self.request.session['clients'] = self.request.GET
            saved_request = self.request.session.get('clients', self.request.GET)
            self.request.GET = saved_request
            queryset = ClientFilter(saved_request, queryset=Client.objects.all()).qs
            paginator = Paginator(queryset, 25)
            page = self.request.GET.get('page')
            try:
                queryset = paginator.page(page)
            except PageNotAnInteger:
                queryset = paginator.page(1)
            except EmptyPage:
                queryset = paginator.page(paginator.num_pages)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ClientList, self).get_context_data(**kwargs)
        context['clients'] = list(Client.objects.all().values_list('name', flat=True))
        return context


class ClientDetail(LoginRequiredMixin, DetailView):
    template_name = 'client_detail.html'
    model = Client


class ClientCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'new_client.html'
    model = Client
    success_url = '/clients/'
    success_message = 'Klientas sėkmingai įvestas!'
    fields = ['name', 'client_type', 'code', 'telephone', 'fax', 'email', 'description', 'reg_street',
              'reg_city', 'reg_post_code', 'reg_country', 'corr_street', 'corr_city', 'corr_post_code', 'corr_country']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ClientCreate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        form.instance.added_by = self.request.user.userextended
        return super(ClientCreate, self).form_valid(form)


class ClientUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'update_client.html'
    model = Client
    success_url = '/clients/'
    success_message = 'Klientas sėkmingai atnaujintas!'
    fields = ['name', 'client_type', 'code', 'telephone', 'fax', 'email', 'description', 'reg_street',
              'reg_city', 'reg_post_code', 'reg_country', 'corr_street', 'corr_city', 'corr_post_code', 'corr_country']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ClientUpdate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        form.instance.added_by = self.request.user.userextended
        return super(ClientUpdate, self).form_valid(form)


class ClientDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Client
    success_message = 'Klientas sėkmingai ištrintas!'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ClientDelete, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return self.request.GET['next'] if self.request.GET['next'] else '/clients/'


class ApartmentList(LoginRequiredMixin, ListView):
    template_name = 'apartment_list.html'

    def get_queryset(self, **kwargs):
        if self.request.user.is_superuser:
            if self.request.GET:
                self.request.session['apartments'] = self.request.GET
            saved_request = self.request.session.get('apartments', self.request.GET)
            self.request.GET = saved_request
            queryset = ApartmentFilter(saved_request)
            paginator = Paginator(queryset, 25)
            page = self.request.GET.get('page')
            try:
                queryset = paginator.page(page)
            except PageNotAnInteger:
                queryset = paginator.page(1)
            except EmptyPage:
                queryset = paginator.page(paginator.num_pages)
        else:
            # queryset = Building.objects.filter(added_by=self.request.user.userextended)
            if self.request.GET:
                self.request.session['apartments'] = self.request.GET
            saved_request = self.request.session.get('apartments', self.request.GET)
            self.request.GET = saved_request
            queryset = ApartmentFilter(saved_request, queryset=Apartment.objects.all()).qs
            paginator = Paginator(queryset, 25)
            page = self.request.GET.get('page')
            try:
                queryset = paginator.page(page)
            except PageNotAnInteger:
                queryset = paginator.page(1)
            except EmptyPage:
                queryset = paginator.page(paginator.num_pages)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ApartmentList, self).get_context_data(**kwargs)
        context['buildings'] = list(Building.objects.all().values_list('name', flat=True))
        return context


class ApartmentDetail(LoginRequiredMixin, DetailView):
    template_name = 'apartment_detail.html'
    model = Apartment

    def get_context_data(self, **kwargs):
        context = super(ApartmentDetail, self).get_context_data(**kwargs)
        apartment = self.get_object()
        context['buildings'] = Building.objects.all()
        context['apartments'] = Apartment.objects.all()
        context['clients'] = Client.objects.all()
        schedules = []
        if len(apartment.loan_set.all()) > 0:
            loans = list(apartment.loan_set.all())
            main_loan = None
            for loan in loans:
                if not loan.accrued_interest:
                    main_loan = loan
                    loans.remove(loan)
            if not main_loan:
                context['no_loan'] = True
                main_loan = loans[0]
            complete_schedule = create_repayment_schedule(main_loan)
            for loan in loans:
                schedules.append(create_repayment_schedule(loan))
            for i in range(len(schedules)):
                j = 0
                for dates, credit_balance, interest, sum_to_pay, loan_repayments, loan_repayments_done, interests_done, total_done, is_due in schedules[i]:
                    if len(complete_schedule) > j:
                        complete_schedule[j][1] += credit_balance if complete_schedule[j][1] > 0 else 0
                        complete_schedule[j][2] += interest
                        complete_schedule[j][3] += sum_to_pay if complete_schedule[j][3] > 0 else 0
                        complete_schedule[j][4] += loan_repayments if complete_schedule[j][4] > 0 else 0
                        # complete_schedule[j][5] += loan_repayments_done
                        complete_schedule[j][6] += interests_done
                        complete_schedule[j][7] += total_done
                        complete_schedule[j][8] += is_due
                        j += 1
            loan_repayments_total = 0
            interest_total = 0
            sum_to_pay_total = 0
            for dates, credit_balance, interest, sum_to_pay, loan_repayments, loan_repayments_done, interests_done, total_done, is_due in complete_schedule:
                loan_repayments_total += loan_repayments
                interest_total += interest
                sum_to_pay_total += sum_to_pay
            context['schedule'] = complete_schedule
            context['loan_repayments_total'] = loan_repayments_total
            context['interest_total'] = interest_total
            context['sum_to_pay_total'] = sum_to_pay_total
        total_interest = 0
        total_repayment = 0
        total_early_repayment = 0
        total_compensated = 0
        total_overpaid = 0
        for payment in apartment.payment_set.all():
            if payment.payment_type == '0':
                total_repayment += payment.payment_sum
            elif payment.payment_type == '1':
                total_interest += payment.payment_sum
            elif payment.payment_type == '2':
                total_early_repayment += payment.payment_sum
            elif payment.payment_type == '5':
                total_compensated += payment.payment_sum
            elif payment.payment_type == '6':
                total_overpaid += payment.payment_sum
        total_sum = total_interest + total_repayment + total_early_repayment + total_compensated
        context['total_interest'] = total_interest
        context['total_repayment'] = total_repayment
        context['total_early_repayment'] = total_early_repayment
        context['total_compensated'] = total_compensated
        context['total_sum'] = total_sum
        context['total_overpaid'] = total_overpaid
        return context


class ApartmentCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'new_apartment.html'
    model = Apartment
    success_url = '/apartments/'
    success_message = 'Butas sėkmingai įvestas!'
    fields = ['code', 'apartment_no', 'area', 'floor', 'description', 'building', 'client', 'own_funds']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ApartmentCreate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        form.instance.added_by = self.request.user.userextended
        return super(ApartmentCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ApartmentCreate, self).get_context_data(**kwargs)
        context['buildings'] = Building.objects.all()
        context['clients'] = Client.objects.all()
        return context


class ApartmentUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'update_apartment.html'
    model = Apartment
    success_url = '/apartments/'
    success_message = 'Butas sėkmingai atnaujintas!'
    fields = ['code', 'apartment_no', 'area', 'floor', 'description', 'building', 'client', 'own_funds']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ApartmentUpdate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        form.instance.added_by = self.request.user.userextended
        return super(ApartmentUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ApartmentUpdate, self).get_context_data(**kwargs)
        context['buildings'] = Building.objects.all()
        context['clients'] = Client.objects.all()
        return context


class ApartmentDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Apartment
    success_message = 'Butas sėkmingai ištrintas!'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ApartmentDelete, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return self.request.GET['next'] if self.request.GET['next'] else '/apartments/'


class LoanList(LoginRequiredMixin, ListView):
    template_name = 'loan_list.html'

    def get_queryset(self, **kwargs):
        if self.request.user.is_superuser:
            if self.request.GET:
                self.request.session['loans'] = self.request.GET
            saved_request = self.request.session.get('loans', self.request.GET)
            self.request.GET = saved_request
            queryset = LoanFilter(saved_request)
            paginator = Paginator(queryset, 25)
            page = self.request.GET.get('page')
            try:
                queryset = paginator.page(page)
            except PageNotAnInteger:
                queryset = paginator.page(1)
            except EmptyPage:
                queryset = paginator.page(paginator.num_pages)
        else:
            # queryset = Building.objects.filter(added_by=self.request.user.userextended)
            if self.request.GET:
                self.request.session['loans'] = self.request.GET
            saved_request = self.request.session.get('loans', self.request.GET)
            self.request.GET = saved_request
            queryset = LoanFilter(saved_request, queryset=Loan.objects.all()).qs
            paginator = Paginator(queryset, 25)
            page = self.request.GET.get('page')
            try:
                queryset = paginator.page(page)
            except PageNotAnInteger:
                queryset = paginator.page(1)
            except EmptyPage:
                queryset = paginator.page(paginator.num_pages)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(LoanList, self).get_context_data(**kwargs)
        context['buildings'] = list(Building.objects.all().values_list('name', flat=True))
        return context


class LoanDetail(LoginRequiredMixin, DetailView):
    template_name = 'loan_detail.html'
    model = Loan

    def get_context_data(self, **kwargs):
        context = super(LoanDetail, self).get_context_data(**kwargs)
        loan = context['object']
        context['schedule'] = create_repayment_schedule(loan)
        total_interest = 0
        for dates, credit_balance, interest, sum_to_pay, loan_repayments, loan_repayments_done, interests_done, total_done, is_due in context['schedule']:
            total_interest += interest
        context['total_interest'] = total_interest
        context['monthly_payment'] = loan.monthly_payment(loan.outstanding_loan_balance(datetime.now().date()), loan.number_of_periods)
        context['outstanding_loan_balance'] = loan.outstanding_loan_balance(datetime.now().date())

        # context['monthly_credit_repayment'] = round(loan.loan_sum / loan.number_of_periods, 2)
        return context


class LoanCreate(LoginRequiredMixin, CreateView):
    template_name = 'new_loan.html'
    model = Loan
    success_url = '/loans/'
    success_message = 'Paskola sėkmingai įvesta!'
    fields = ['contract_no', 'loan_type', 'description', 'building', 'apartment', 'loan_sum2',
              'interest_rate', 'currency', 'postponed_months', 'loans', 'valid_from', 'valid_to', 'accrued_interest',
              'payment_day', 'days_interest_is_payable', 'calculate_fine', 'created_at']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(LoanCreate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        form.instance.added_by = self.request.user.userextended
        return super(LoanCreate, self).form_valid(form)

    # def form_invalid(self, form):
    #     """
    #     If the form is invalid, re-render the context data with the
    #     data-filled form and errors.
    #     """
    #     return self.render_to_response(self.get_context_data(form=form))

    def get_context_data(self, **kwargs):
        context = super(LoanCreate, self).get_context_data(**kwargs)
        context['buildings'] = Building.objects.all()
        context['apartments'] = Apartment.objects.all()
        context['loans'] = Loan.objects.all()
        return context


class LoanUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'update_loan.html'
    model = Loan
    success_url = '/loans/'
    success_message = 'Paskola sėkmingai atnaujinta!'
    fields = ['contract_no', 'loan_type', 'description', 'building', 'apartment', 'loan_sum2',
              'interest_rate', 'currency', 'postponed_months', 'loans', 'valid_from', 'valid_to', 'accrued_interest',
              'payment_day', 'days_interest_is_payable', 'calculate_fine', 'created_at']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(LoanUpdate, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(LoanUpdate, self).get_context_data(**kwargs)
        context['buildings'] = Building.objects.all()
        context['apartments'] = Apartment.objects.all()
        context['loans'] = Loan.objects.all()
        return context

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        form.instance.added_by = self.request.user.userextended

        if form.instance.building:
            for apartment in form.instance.building.apartment_set.all():
                for loan in apartment.loan_set.all():
                    loan.created_at = form.instance.created_at
                    loan.valid_from = form.instance.valid_from
                    loan.valid_to = form.instance.valid_to
                    loan.save()
            for loan in form.instance.building.loan_set.all():
                if form.instance is not loan:
                    loan.created_at = form.instance.created_at
                    loan.valid_from = form.instance.valid_from
                    loan.valid_to = form.instance.valid_to
                    loan.save()
        if form.instance.apartment:
            for loan in form.instance.apartment.loan_set.all():
                if form.instance is not loan:
                    loan.created_at = form.instance.created_at
                    loan.valid_from = form.instance.valid_from
                    loan.valid_to = form.instance.valid_to
                    loan.save()
        return super(LoanUpdate, self).form_valid(form)


class LoanDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Loan
    success_message = 'Paskola sėkmingai ištrinta!'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(LoanDelete, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return self.request.GET['next'] if self.request.GET['next'] else '/loans/'


class LoanSplit(LoginRequiredMixin, RedirectView):
    url = '/'
    permanent = True

    def get(self, request, *args, **kwargs):
        if request.GET['next']:
            self.url = request.GET['next']
        loan_to_split = Loan.objects.get(pk=kwargs['pk'])
        increment = 0
        loan_sum = loan_to_split.loan_sum / len(loan_to_split.building.apartment_set.all())
        for apartment in loan_to_split.building.apartment_set.all():
            increment += 1
            Loan(contract_no=loan_to_split.contract_no + '-' + str(increment), loan_type=loan_to_split.loan_type,
                 apartment=apartment, loan_sum=loan_sum, interest_rate=loan_to_split.interest_rate,
                 currency=loan_to_split.currency, postponed_months=loan_to_split.postponed_months,
                 valid_from=loan_to_split.valid_from, valid_to=loan_to_split.valid_to,
                 accrued_interest=loan_to_split.accrued_interest, calculate_fine=loan_to_split.calculate_fine,
                 payment_day=loan_to_split.payment_day, days_interest_is_payable=loan_to_split.days_interest_is_payable,
                 added_by=request.user.userextended, loans=loan_to_split, created_at=loan_to_split.created_at).save()
        return super(LoanSplit, self).get(request, *args, **kwargs)


class RepaymentSchedule(LoginRequiredMixin, DetailView):
    model = Loan
    template_name = 'repayment_schedule.html'

    def get_queryset(self):
        return Loan.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(RepaymentSchedule, self).get_context_data(**kwargs)
        loan = self.get_queryset()
        context['object'] = loan
        schedule = create_repayment_schedule(loan, document=True)
        context['schedule'] = schedule
        total_interest, total_repayment, total_sum = 0, 0, 0
        for repayment in schedule:
            total_interest += repayment[2]
            total_repayment += repayment[4]
            total_sum += repayment[3]
        context['total_interest'] = total_interest
        context['total_repayment'] = total_repayment
        context['total_sum'] = total_interest + total_repayment
        return context

    def get(self, request, *args, **kwargs):
        self.object = None
        htmlstring = render_to_string(self.template_name, self.get_context_data(), context_instance=RequestContext(self.request))
        html = HTML(string=htmlstring)
        main_doc = html.render()
        pdf = main_doc.write_pdf()
        return HttpResponse(pdf, content_type='application/pdf')


class RepaymentScheduleCombined(LoginRequiredMixin, TemplateView):
    template_name = 'repayment_schedule_combined.html'

    def get_context_data(self, **kwargs):
        context = super(RepaymentScheduleCombined, self).get_context_data(**kwargs)
        if self.kwargs['type'] == '0':
            building = Building.objects.get(pk=self.kwargs['pk'])
            loans = [loan for loan in building.loan_set.all()]
            context['receiver'] = building.name
            context['extra_requisites'] = building.extra_requisites
        elif self.kwargs['type'] == '1':
            building = Apartment.objects.get(pk=self.kwargs['pk'])
            loans = [loan for loan in building.loan_set.all()]
            context['receiver'] = building.code
            context['extra_requisites'] = building.building.extra_requisites
        context['contract_no_single'] = loans[0].contract_no if not loans[0].accrued_interest else loans[1]
        context['valid_single'] = loans[0].valid_from
        context['contract_no'] = ", ".join([loan.contract_no for loan in loans])
        context['valid'] = ", ".join([str(loan.valid_from) + ' - ' + str(loan.valid_to) for loan in loans])
        context['loan_sum'] = sum([loan.loan_sum for loan in loans])
        context['interest_rate'] = ", ".join([str(loan.interest_rate) for loan in loans])
        schedules = []
        if len(building.loan_set.all()) > 0:
            complete_schedule = create_repayment_schedule(building.loan_set.all()[0], document=True)
            for loan in building.loan_set.all():
                schedules.append(create_repayment_schedule(loan, document=True))
            for i in range(len(schedules)):
                if i > 0:
                    j = 0
                    for dates, credit_balance, interest, sum_to_pay, loan_repayments, _, _, _, _ in schedules[i]:
                        if len(complete_schedule) > j:
                            complete_schedule[j][1] += credit_balance if credit_balance > 0 and complete_schedule[j][1] > 0 else 0
                            complete_schedule[j][2] += interest if interest > 0 else 0 #and complete_schedule[j][2] > 0
                            complete_schedule[j][3] += sum_to_pay if sum_to_pay > 0 and complete_schedule[j][3] > 0 else 0
                            complete_schedule[j][4] += loan_repayments if loan_repayments > 0 and complete_schedule[j][4] > 0 else 0
                            j += 1
            context['schedule'] = complete_schedule
            total_interest, total_repayment, total_sum = 0, 0, 0
            for repayment in complete_schedule:
                total_interest += repayment[2]
                total_repayment += repayment[4]
                total_sum += repayment[3]
            context['total_interest'] = total_interest
            context['total_repayment'] = total_repayment
            context['total_sum'] = total_interest + total_repayment
        return context

    def get(self, request, *args, **kwargs):
        htmlstring = render_to_string(self.template_name, self.get_context_data(), context_instance=RequestContext(self.request))
        html = HTML(string=htmlstring)
        main_doc = html.render()
        pdf = main_doc.write_pdf()
        return HttpResponse(pdf, content_type='application/pdf')


class BillList(LoginRequiredMixin, ListView):
    template_name = 'bill_list.html'

    def get_queryset(self, **kwargs):
        if self.request.user.is_superuser:
            queryset = Bill.objects.all()
            paginator = Paginator(queryset, 25)
            page = self.request.GET.get('page')
            try:
                queryset = paginator.page(page)
            except PageNotAnInteger:
                queryset = paginator.page(1)
            except EmptyPage:
                queryset = paginator.page(paginator.num_pages)
        else:
            # queryset = Building.objects.filter(added_by=self.request.user.userextended)
            queryset = Bill.objects.all()
            paginator = Paginator(queryset, 25)
            page = self.request.GET.get('page')
            try:
                queryset = paginator.page(page)
            except PageNotAnInteger:
                queryset = paginator.page(1)
            except EmptyPage:
                queryset = paginator.page(paginator.num_pages)
        return queryset


class BillDetail(LoginRequiredMixin, DetailView):
    template_name = 'bill_detail.html'
    model = Bill


class BillCreate(LoginRequiredMixin, CreateView):
    template_name = 'new_bill.html'
    model = Bill
    success_url = '/bills/'
    success_message = 'Sąskaita sėkmingai įvesta!'
    fields = ['bill_no', 'building', 'apartment', 'client', 'from_date', 'to_date', 'sent', 'note', 'paid']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BillCreate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        form.instance.added_by = self.request.user.userextended
        bill_sum = 0
        bill_details = []
        for loan in Loan.objects.filter(apartment=form.cleaned_data['apartment']):
            calculated_payments = calculate_payment_between_dates(create_repayment_schedule(loan),
                                                                  form.instance.from_date, form.instance.to_date)
            bill_sum += calculated_payments[2]
            bill_details.append(BillDetails(added_by=self.request.user.userextended,
                                            name=loan.contract_no + ': Palūkanos',
                                            bill=form.instance,
                                            bill_sum=calculated_payments[0]))
            bill_details.append(BillDetails(added_by=self.request.user.userextended,
                                            name=loan.contract_no + ': Investicijų grąža',
                                            bill=form.instance,
                                            bill_sum=calculated_payments[1]))
        form.instance.bill_sum = bill_sum
        form.save()
        for bill in bill_details:
            bill.bill = form.instance
            bill.save()
        return super(BillCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BillCreate, self).get_context_data(**kwargs)
        context['buildings'] = Building.objects.all()
        context['apartments'] = Apartment.objects.all()
        context['clients'] = Client.objects.all()
        return context


class BillUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'update_bill.html'
    model = Bill
    success_url = '/bills/'
    success_message = 'Sąskaita sėkmingai atnaujinta!'
    fields = ['bill_no', 'building', 'apartment', 'client', 'from_date', 'to_date', 'sent', 'note', 'paid']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BillUpdate, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BillUpdate, self).get_context_data(**kwargs)
        context['buildings'] = Building.objects.all()
        context['apartments'] = Apartment.objects.all()
        context['clients'] = Client.objects.all()
        return context

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        BillDetails.objects.filter(bill=form.instance).delete()
        form.instance.added_by = self.request.user.userextended
        bill_sum = 0
        bill_details = []
        for loan in Loan.objects.filter(apartment=form.cleaned_data['apartment']):
            calculated_payments = calculate_payment_between_dates(create_repayment_schedule(loan), form.instance.from_date, form.instance.to_date)
            bill_sum += calculated_payments[2]
            bill_details.append(BillDetails(added_by=self.request.user.userextended, name=loan.contract_no + ': Palūkanos', bill=form.instance, bill_sum=calculated_payments[0]))
            bill_details.append(BillDetails(added_by=self.request.user.userextended, name=loan.contract_no + ': Investicijų grąža', bill=form.instance, bill_sum=calculated_payments[1]))
        form.instance.bill_sum = bill_sum
        form.save()
        for bill in bill_details:
            bill.bill = form.instance
            bill.save()
        return super(BillUpdate, self).form_valid(form)


class BillDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Bill
    success_message = 'Sąskaita sėkmingai ištrinta!'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BillDelete, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return self.request.GET['next'] if self.request.GET['next'] else '/bills/'


class PaymentList(LoginRequiredMixin, ListView):
    template_name = 'payment_list.html'
    full_queryset = []

    def get_queryset(self, **kwargs):
        if self.request.user.is_superuser:
            if self.request.GET:
                self.request.session['payments'] = self.request.GET
            saved_request = self.request.session.get('payments', self.request.GET)
            self.request.GET = saved_request
            queryset = PaymentFilter(saved_request)
            paginator = Paginator(queryset, 25)
            page = self.request.GET.get('page')
            try:
                queryset = paginator.page(page)
            except PageNotAnInteger:
                queryset = paginator.page(1)
            except EmptyPage:
                queryset = paginator.page(paginator.num_pages)
        else:
            # queryset = Building.objects.filter(added_by=self.request.user.userextended)
            if self.request.GET:
                self.request.session['payments'] = self.request.GET
            saved_request = self.request.session.get('payments', self.request.GET)
            self.request.GET = saved_request
            queryset = PaymentFilter(saved_request, queryset=Payment.objects.all()).qs
            self.full_queryset = queryset
            paginator = Paginator(queryset, 25)
            page = self.request.GET.get('page')
            try:
                queryset = paginator.page(page)
            except PageNotAnInteger:
                queryset = paginator.page(1)
            except EmptyPage:
                queryset = paginator.page(paginator.num_pages)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(PaymentList, self).get_context_data(**kwargs)
        total_interest = 0
        total_repayment = 0
        total_early_repayment = 0
        for payment in self.full_queryset:
            if payment.payment_type == '0':
                total_repayment += payment.payment_sum
            elif payment.payment_type == '1':
                total_interest += payment.payment_sum
            elif payment.payment_type == '2':
                total_early_repayment += payment.payment_sum
        total_sum = total_interest + total_repayment
        context['total_interest'] = total_interest
        context['total_repayment'] = total_repayment
        context['total_sum'] = total_sum
        context['apartments'] = list(Apartment.objects.all().values_list('code', flat=True))
        context['buildings'] = list(Building.objects.all().values_list('name', flat=True))
        return context


class PaymentDetail(LoginRequiredMixin, DetailView):
    template_name = 'payment_detail.html'
    model = Payment


class PaymentCreate(LoginRequiredMixin, CreateView):
    template_name = 'new_payment.html'
    model = Payment
    success_url = '/payments/'
    success_message = 'Mokėjimas sėkmingai įvestas!'
    fields = ['name', 'payment_date', 'apartment', 'building', 'payment_type', 'payment_sum', 'currency']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(PaymentCreate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.added_by = self.request.user.userextended
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        if form.instance.payment_type == '4':
            automatic_payments(form.instance.apartment, form.instance.payment_date, form.instance.payment_sum,
                               form.instance.building, form.instance.currency, form.instance.added_by, form.instance.name)
            return HttpResponseRedirect(self.get_success_url())
        return super(PaymentCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PaymentCreate, self).get_context_data(**kwargs)
        context['apartments'] = Apartment.objects.all()
        return context

    def get_success_url(self):
        return self.request.GET['next'] if self.request.GET['next'] else '/payments/'


class PaymentUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'update_payment.html'
    model = Payment
    success_url = '/payments/'
    success_message = 'Mokėjimas sėkmingai atnaujintas!'
    fields = ['name', 'payment_date', 'apartment', 'building', 'payment_type', 'payment_sum', 'currency']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(PaymentUpdate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        form.instance.added_by = self.request.user.userextended
        return super(PaymentUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PaymentUpdate, self).get_context_data(**kwargs)
        context['apartments'] = Apartment.objects.all()
        return context


class PaymentDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Payment
    success_message = 'Mokėjimas sėkmingai ištrintas!'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(PaymentDelete, self).dispatch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
            if self.object.auto_id:
                Payment.objects.filter(auto_id=self.object.auto_id).delete()
            self.object.delete()
        except:
            pass
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return self.request.GET['next'] if self.request.GET['next'] else '/payments/'


class BillView(LoginRequiredMixin, DetailView):
    template_name = '1447238659.htm'
    model = Bill

    def get_context_data(self, **kwargs):
        context = super(BillView, self).get_context_data(**kwargs)
        return context


class CompensationCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Compensation
    fields = ['compensation_no', 'compensation_approved_date', 'date_to', 'date_from', 'apartment']
    success_url = '/apartments/'
    success_message = 'Kompensacijos pažyma sėkmingai sukurta!'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CompensationCreate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        form.instance.added_by = self.request.user.userextended
        return super(CompensationCreate, self).form_valid(form)


class CompensationUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'update_compensation.html'
    model = Compensation
    success_url = '/apartments/'
    success_message = 'Kompensacijos pažyma sėkmingai atnaujinta!'
    fields = ['apartment', 'date_from', 'date_to', 'compensation_approved_date', 'compensation_no']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CompensationUpdate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if self.request.GET['next']:
            self.success_url = self.request.GET['next']
        form.instance.added_by = self.request.user.userextended
        return super(CompensationUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CompensationUpdate, self).get_context_data(**kwargs)
        context['apartments'] = Apartment.objects.all()
        return context


class CompensationDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Compensation
    success_message = 'Kompensacijos pažyma sėkmingai ištrinta!'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CompensationDelete, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return self.request.GET['next'] if self.request.GET['next'] else '/apartments/'


class CompensationView(LoginRequiredMixin, TemplateView):
    template_name = 'compensation.html'

    def get_queryset(self, **kwargs):
        # date_from = self.request.GET['date_from']
        # date_to = self.request.GET['date_to']
        start = datetime.strptime(self.request.GET['start'], '%Y-%m-%d').date()
        end = datetime.strptime(self.request.GET['end'], '%Y-%m-%d').date()
        queryset = Compensation.objects.filter(apartment__building=self.request.GET['building'], date_from__lte=start,
                                               date_to__gte=end)
        return queryset

    def get_context_data(self, **kwargs):
        start = datetime.strptime(self.request.GET['start'], '%Y-%m-%d').date()
        end = datetime.strptime(self.request.GET['end'], '%Y-%m-%d').date()
        context = super(CompensationView, self).get_context_data(**kwargs)
        context['building'] = Building.objects.get(pk=self.request.GET['building'])
        compensations = self.get_queryset()
        compensation_sums = [calculate_compensation_sums(compensation.apartment, start, end) for compensation in compensations]
        context['object_list'] = zip(compensations, range(len(compensations)), compensation_sums)
        total_credit_balance = 0
        total_credit_to_pay = 0
        total_interest_to_pay = 0
        total_to_pay = 0
        for compensation in compensation_sums:
            try:
                total_credit_balance += compensation[3]
                total_credit_to_pay += compensation[1]
                total_interest_to_pay += compensation[2]
                total_to_pay += compensation[0]
            except:
                pass
        date = datetime.strptime(self.request.GET['date'], '%Y-%m-%d').date()
        context['total_credit_balance'] = total_credit_balance
        context['total_credit_to_pay'] = total_credit_to_pay
        context['total_interest_to_pay'] = total_interest_to_pay
        context['total_to_pay'] = total_to_pay
        context['total_in_words'] = num2words(int(total_to_pay), lang='lt').capitalize() + ' eurai ir ' + str(int((total_to_pay - int(total_to_pay)) * 100)) + ' euro centai'
        context['date'] = date
        return context

    def get(self, request, *args, **kwargs):
        htmlstring = render_to_string(self.template_name, self.get_context_data(), context_instance=RequestContext(self.request))
        html = HTML(string=htmlstring, base_url=request.build_absolute_uri())
        main_doc = html.render()
        pdf = main_doc.write_pdf()
        return HttpResponse(pdf, content_type='application/pdf')


class FineView(LoginRequiredMixin, TemplateView):
    template_name = 'fines.html'

    def get_context_data(self, **kwargs):
        context = super(FineView, self).get_context_data(**kwargs)
        context['fines'] = fine_list(building=self.request.GET['building'], to=datetime.strptime(self.request.GET['to'], '%Y-%m-%d').date())
        context['to'] = self.request.GET.get('to', '')
        return context

    def get(self, request, *args, **kwargs):
        htmlstring = render_to_string(self.template_name, self.get_context_data(), context_instance=RequestContext(self.request))
        html = HTML(string=htmlstring)
        main_doc = html.render()
        pdf = main_doc.write_pdf()
        return HttpResponse(pdf, content_type='application/pdf')


class FineViewSB(LoginRequiredMixin, TemplateView):
    template_name = 'fines_sb.html'

    def get_context_data(self, **kwargs):
        context = super(FineViewSB, self).get_context_data(**kwargs)
        building = Building.objects.get(pk=self.request.GET['building'])
        context['building'] = building
        context['fines'] = fine_list(building=building, to=datetime.strptime(self.request.GET['to'], '%Y-%m-%d').date())
        context['to'] = self.request.GET.get('to', '')
        context['date'] = self.request.GET.get('date', '')
        return context

    # def get(self, request, *args, **kwargs):
    #     htmlstring = render_to_string(self.template_name, self.get_context_data(), context_instance=RequestContext(self.request))
    #     html = HTML(string=htmlstring)
    #     main_doc = html.render()
    #     pdf = main_doc.write_pdf()
    #     return HttpResponse(pdf, content_type='application/pdf')


class ResetFine(LoginRequiredMixin, RedirectView):
    permanent = True

    def get(self, request, *args, **kwargs):
        apartment = Apartment.objects.get(pk=kwargs['pk'])
        apartment.fine = 0
        apartment.save()
        self.url = '/apartments/%s' % kwargs['pk']
        return super(ResetFine, self).get(request, *args, **kwargs)


class DocumentsView(LoginRequiredMixin, TemplateView):
    template_name = 'documents.html'

    def get_context_data(self, **kwargs):
        context = super(DocumentsView, self).get_context_data(**kwargs)
        context['buildings'] = Building.objects.all()
        context['apartments'] = Apartment.objects.all()
        return context


class LoanManagementView(LoginRequiredMixin, TemplateView):
    template_name = 'loan_management.html'
    new_apartment = None

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(LoanManagementView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(LoanManagementView, self).get_context_data(**kwargs)
        context['loans'] = Loan.objects.all()
        context['apartments'] = Apartment.objects.all()
        context['new_apartment'] = self.new_apartment
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()
        if self.request.POST['action'] == 'separate':
            pass
        else:
            apartments = [Apartment.objects.get(pk=apartment_pk) for apartment_pk in self.request.POST.getlist('apartment')]
            area = 0
            fine = 0
            for apartment in apartments:
                area += apartment.area
                fine += apartment.current_fine()
            new_apartment = Apartment(code=apartments[0].code, apartment_no=apartments[0].apartment_no, area=area,
                                      floor=apartments[0].floor, description=apartments[0].description,
                                      building=apartments[0].building, client=apartments[0].client,
                                      own_funds=apartments[0].own_funds, fine=fine,
                                      added_by=self.request.user.userextended)
            new_apartment.save()
            for loan in apartments[0].loan_set.all():
                if loan.accrued_interest:
                    accrued_loan = loan
                else:
                    main_loan = loan
            valid_from = self.request.POST['valid_from']
            valid_to = self.request.POST['valid_to']
            loan_sum_accrued = 0
            loan_sum_main = 0
            for apartment in apartments:
                for loan in apartment.loan_set.all():
                    if loan.accrued_interest:
                        loan_sum_accrued += loan.loan_sum
                    else:
                        loan_sum_main += loan.loan_sum
            accrued_loan_new = Loan(added_by=self.request.user.userextended, contract_no=accrued_loan.contract_no,
                                    loan_type=accrued_loan.loan_type, apartment=new_apartment,
                                    loan_sum=loan_sum_accrued, interest_rate=accrued_loan.interest_rate,
                                    currency=accrued_loan.currency, postponed_months=accrued_loan.postponed_months,
                                    loans=accrued_loan.loans, valid_from=valid_from, valid_to=valid_to,
                                    accrued_interest=accrued_loan.accrued_interest, payment_day=accrued_loan.payment_day,
                                    days_interest_is_payable=accrued_loan.days_interest_is_payable,
                                    calculate_fine=accrued_loan.calculate_fine, description=accrued_loan.description,
                                    building=None)
            accrued_loan_new.save()
            main_loan_new = Loan(added_by=self.request.user.userextended, contract_no=main_loan.contract_no,
                                 loan_type=main_loan.loan_type, apartment=new_apartment, loan_sum=loan_sum_main,
                                 interest_rate=main_loan.interest_rate, currency=main_loan.currency,
                                 postponed_months=main_loan.postponed_months, loans=main_loan.loans,
                                 valid_from=valid_from, valid_to=valid_to, accrued_interest=main_loan.accrued_interest,
                                 payment_day=main_loan.payment_day,
                                 days_interest_is_payable=main_loan.days_interest_is_payable,
                                 calculate_fine=main_loan.calculate_fine, description=accrued_loan.description,
                                 building=None)
            main_loan_new.save()
            # [Apartment.objects.get(pk=apartment_pk).delete() for apartment_pk in self.request.POST.getlist('apartment')]
            self.new_apartment = new_apartment
            context['new_accrued'] = accrued_loan_new
            context['new_main'] = main_loan_new
        return super(LoanManagementView, self).render_to_response(context)


class ProfileView(LoginRequiredMixin, DetailView):
    template_name = 'profile_view.html'
    model = UserExtended

@csrf_exempt
def login_user(request):
    if request.method == 'GET':
        if request.user.is_authenticated():
            return redirect('building_list')
        else:
            return render(request, 'login.html', {})
    elif request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('building_list')
            else:
                return redirect('login_user')
        else:
            return redirect('login_user')


def home(request):
    if request.user.is_authenticated():
        return redirect('building_list')
    else:
        return redirect('login_user')
