# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('turtas', '0003_auto_20160114_1550'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apartment',
            name='added_by',
            field=models.ForeignKey(blank=True, null=True, to='turtas.UserExtended'),
        ),
        migrations.AlterField(
            model_name='apartment',
            name='building',
            field=models.ForeignKey(blank=True, null=True, to='turtas.Building'),
        ),
        migrations.AlterField(
            model_name='apartment',
            name='client',
            field=models.ForeignKey(blank=True, null=True, to='turtas.Client'),
        ),
        migrations.AlterField(
            model_name='bill',
            name='added_by',
            field=models.ForeignKey(blank=True, null=True, to='turtas.UserExtended'),
        ),
        migrations.AlterField(
            model_name='bill',
            name='apartment',
            field=models.ForeignKey(blank=True, null=True, to='turtas.Apartment'),
        ),
        migrations.AlterField(
            model_name='bill',
            name='building',
            field=models.ForeignKey(blank=True, null=True, to='turtas.Building'),
        ),
        migrations.AlterField(
            model_name='bill',
            name='client',
            field=models.ForeignKey(blank=True, null=True, to='turtas.Client'),
        ),
        migrations.AlterField(
            model_name='billdetails',
            name='added_by',
            field=models.ForeignKey(blank=True, null=True, to='turtas.UserExtended'),
        ),
        migrations.AlterField(
            model_name='billdetails',
            name='bill',
            field=models.ForeignKey(blank=True, null=True, to='turtas.Bill'),
        ),
        migrations.AlterField(
            model_name='building',
            name='added_by',
            field=models.ForeignKey(blank=True, null=True, to='turtas.UserExtended'),
        ),
        migrations.AlterField(
            model_name='client',
            name='added_by',
            field=models.ForeignKey(blank=True, null=True, to='turtas.UserExtended'),
        ),
        migrations.AlterField(
            model_name='compensation',
            name='added_by',
            field=models.ForeignKey(blank=True, null=True, to='turtas.UserExtended'),
        ),
        migrations.AlterField(
            model_name='compensation',
            name='apartment',
            field=models.ForeignKey(blank=True, null=True, to='turtas.Apartment'),
        ),
        migrations.AlterField(
            model_name='loan',
            name='added_by',
            field=models.ForeignKey(blank=True, null=True, to='turtas.UserExtended'),
        ),
        migrations.AlterField(
            model_name='loan',
            name='apartment',
            field=models.ForeignKey(blank=True, null=True, to='turtas.Apartment'),
        ),
        migrations.AlterField(
            model_name='loan',
            name='building',
            field=models.ForeignKey(blank=True, null=True, to='turtas.Building'),
        ),
        migrations.AlterField(
            model_name='loan',
            name='loans',
            field=models.ForeignKey(blank=True, null=True, to='turtas.Loan'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='added_by',
            field=models.ForeignKey(blank=True, null=True, to='turtas.UserExtended'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='apartment',
            field=models.ForeignKey(blank=True, null=True, to='turtas.Apartment'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='building',
            field=models.ForeignKey(blank=True, null=True, to='turtas.Building'),
        ),
    ]
