# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('turtas', '0007_auto_20160308_1347'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apartment',
            name='fine',
            field=models.DecimalField(max_digits=10, default=0, decimal_places=2),
        ),
    ]
