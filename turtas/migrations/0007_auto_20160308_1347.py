# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('turtas', '0006_auto_20160307_0730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='building',
            name='apartment_count',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='building',
            name='area',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='building',
            name='floor_count',
            field=models.IntegerField(),
        ),
    ]
