# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('turtas', '0002_auto_20160114_1136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apartment',
            name='floor',
            field=models.CharField(max_length=2, blank=True),
        ),
        migrations.AlterField(
            model_name='loan',
            name='accrued_interest',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='loan',
            name='payment_day',
            field=models.CharField(max_length=2, blank=True),
        ),
        migrations.AlterField(
            model_name='loan',
            name='postponed_months',
            field=models.CharField(max_length=2, blank=True),
        ),
    ]
