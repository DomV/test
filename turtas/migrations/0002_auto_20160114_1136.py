# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('turtas', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apartment',
            name='fine',
            field=models.DecimalField(null=True, max_digits=10, blank=True, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='building',
            name='fine',
            field=models.DecimalField(null=True, max_digits=10, blank=True, decimal_places=2),
        ),
    ]
