# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('turtas', '0008_auto_20160309_2123'),
    ]

    operations = [
        migrations.AddField(
            model_name='loan',
            name='is_split',
            field=models.BooleanField(default=False),
        ),
    ]
