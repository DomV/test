# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('turtas', '0004_auto_20160114_1609'),
    ]

    operations = [
        migrations.AlterField(
            model_name='building',
            name='floor_count',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
