import calendar
import decimal
import math

from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum

from .helpers import calculate_monthdelta, credit_balance_of_apartment, fine_list, credit_balance_of_loan, \
    create_repayment_schedule, calculate_compensation_sums, paid_early_amount_of_apartment


class UserExtended(models.Model):
    KELME = 'Kelme'
    TELSIAI = 'Telsiai'
    TYPE_OF_REPAYMENT_CALCULATION_CHOICES = (
        (KELME, KELME),
        (TELSIAI, TELSIAI)
    )
    user = models.OneToOneField(User)
    full_name = models.CharField(max_length=300, blank=True)
    ceo = models.CharField(max_length=300, blank=True)
    company_description = models.CharField(max_length=300, blank=True)
    company_description_short = models.CharField(max_length=300, blank=True)
    company_district = models.CharField(max_length=300, blank=True)
    bank = models.CharField(max_length=300, blank=True)
    bank_account = models.CharField(max_length=300, blank=True)
    # type_of_repayment_calculation = models.CharField(max_length=20, choices=TYPE_OF_REPAYMENT_CALCULATION_CHOICES,
    #                                                  default=TELSIAI)

    def __str__(self):
        return self.user.username


class Building(models.Model):
    added_by = models.ForeignKey(UserExtended, null=True, blank=True)
    name = models.CharField(max_length=300)
    floor_count = models.IntegerField()
    apartment_count = models.IntegerField()
    address = models.CharField(max_length=300)
    area = models.FloatField()
    description = models.TextField(blank=True)
    city = models.CharField(max_length=30, blank=True)
    renovation_finished_at = models.CharField(max_length=4, blank=True)
    district = models.CharField(max_length=30, blank=True)
    post_code = models.CharField(max_length=10, blank=True)
    country = models.CharField(max_length=30, blank=True)
    extra_requisites = models.TextField(blank=True)
    invoice_note = models.TextField(blank=True)
    fine = models.DecimalField(decimal_places=2, max_digits=10, null=True, blank=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

    def paid_fines(self):
        return Payment.objects.filter(building=self, payment_type='3').aggregate(Sum('payment_sum')).payment_sum__sum

    def ongoing_fine(self):
        fine = fine_list(self)
        if len(fine) > 0:
            return fine[0]['fine']
        else:
            return 0


class Client(models.Model):
    added_by = models.ForeignKey(UserExtended, null=True, blank=True)
    name = models.CharField(max_length=300)
    client_type = models.CharField(max_length=30, blank=True)
    code = models.CharField(max_length=30, blank=True)
    telephone = models.CharField(max_length=10, blank=True)
    fax = models.CharField(max_length=10, blank=True)
    email = models.CharField(max_length=30, blank=True)
    description = models.TextField(blank=True)
    # buildings = models.ManyToManyField(Building, null=True, blank=True)
    reg_street = models.CharField(max_length=300, blank=True)
    reg_city = models.CharField(max_length=30, blank=True)
    reg_post_code = models.CharField(max_length=10, blank=True)
    reg_country = models.CharField(max_length=30, blank=True)
    corr_street = models.CharField(max_length=300, blank=True)
    corr_city = models.CharField(max_length=30, blank=True)
    corr_post_code = models.CharField(max_length=10, blank=True)
    corr_country = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class LoanType(models.Model):
    name = models.CharField(max_length=30)


class Apartment(models.Model):
    added_by = models.ForeignKey(UserExtended, null=True, blank=True)
    code = models.CharField(max_length=300)
    apartment_no = models.CharField(max_length=10, blank=True)
    area = models.FloatField()
    floor = models.CharField(max_length=2, blank=True)
    description = models.TextField(blank=True)
    building = models.ForeignKey(Building, null=True, blank=True)
    client = models.ForeignKey(Client, null=True, blank=True)
    own_funds = models.BooleanField(default=False)
    fine = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    overpaid = models.DecimalField(decimal_places=2, max_digits=10, default=0)

    def __str__(self):
        return self.code

    class Meta:
        ordering = ['code']

    def get_loan_sum(self):
        loans = Loan.objects.filter(apartment=self)
        loan_sum = 0
        for loan in loans:
            loan_sum += loan.loan_sum
        return loan_sum

    def outstanding_loans_balance(self, date):
        loans = Loan.objects.filter(apartment=self)
        loan_sum = 0
        for loan in loans:
            loan_sum += loan.outstanding_loan_balance(date)
        return loan_sum

    def paid_fines(self):
        return Payment.objects.filter(apartment=self, payment_type='3').aggregate(Sum('payment_sum')).payment_sum__sum

    def ongoing_fine(self):
        fine = fine_list(apartment=self)
        if len(fine) > 0 and fine[0]['fine'] > 0:
            payments = self.payment_set.filter(payment_type=3)
            paid = 0
            for payment in payments:
                paid += payment.payment_sum
            return max(fine[0]['fine'] - paid, 0)
        else:
            return 0.00

    def current_fine(self):
        payments = self.payment_set.filter(payment_type=3)
        paid = 0
        for payment in payments:
            paid += payment.payment_sum
        return max(self.fine - paid, 0.00) or 0.00


class Loan(models.Model):
    added_by = models.ForeignKey(UserExtended, null=True, blank=True)
    contract_no = models.CharField(max_length=30)
    loan_type = models.CharField(max_length=30)
    description = models.TextField(blank=True)
    building = models.ForeignKey(Building, null=True, blank=True)
    apartment = models.ForeignKey(Apartment, null=True, blank=True)
    loan_sum2 = models.DecimalField(decimal_places=2, max_digits=10, db_column='loan_sum', default=0)
    interest_rate = models.DecimalField(decimal_places=2, max_digits=5)
    currency = models.CharField(max_length=3)
    postponed_months = models.CharField(max_length=2, blank=True)
    loans = models.ForeignKey('self', null=True, blank=True)
    created_at = models.DateField(blank=True, null=True)
    valid_from = models.DateField(blank=True)
    valid_to = models.DateField(blank=True)
    accrued_interest = models.BooleanField(blank=True, default=False)
    payment_day = models.CharField(max_length=2, blank=True)
    days_interest_is_payable = models.IntegerField(default=365)
    calculate_fine = models.BooleanField(default=True)
    is_split = models.BooleanField(default=False)

    def __str__(self):
        return self.contract_no

    class Meta:
        ordering = ['apartment', 'building']

    def days_in_a_year(self, year):
        return self.days_interest_is_payable + 1 if calendar.isleap(year) else self.days_interest_is_payable

    @property
    def loan_sum(self):
        if self.accrued_interest:
            return self.loan_sum2
        else:
            return self.loan_sum2 - paid_early_amount_of_apartment(self.apartment if self.apartment else self.building)

    @property
    def number_of_periods(self):
        return calculate_monthdelta(self.valid_from, self.valid_to) - 1

    def monthly_payment(self, loan_sum, number_of_periods):
        # if self.building:
        try:
            return round(decimal.Decimal(math.ceil(loan_sum / (number_of_periods + 1) * 100) / 100), 2)  # KELME
        except:
            return 0
        # else:
        #     return round(decimal.Decimal(math.ceil(loan_sum / (number_of_periods + 1) * 100) / 100), 2)

    @property
    def monthly_payment_original(self):
        return self.monthly_payment(self.loan_sum, self.number_of_periods)

    def outstanding_loan_balance(self, date):
        return credit_balance_of_loan(self, date)


class Bill(models.Model):
    added_by = models.ForeignKey(UserExtended, null=True, blank=True)
    bill_no = models.CharField(max_length=300)
    building = models.ForeignKey(Building, null=True, blank=True)
    apartment = models.ForeignKey(Apartment, null=True, blank=True)
    client = models.ForeignKey(Client, null=True, blank=True)
    from_date = models.DateField()
    to_date = models.DateField()
    bill_sum = models.DecimalField(decimal_places=2, max_digits=10, blank=True)
    sent = models.BooleanField(default=False)
    paid = models.BooleanField(default=False)
    note = models.TextField(blank=True)

    def __str__(self):
        return self.bill_no

    class Meta:
        ordering = ['apartment']


class Payment(models.Model):
    added_by = models.ForeignKey(UserExtended, null=True, blank=True)
    name = models.CharField(max_length=300)
    apartment = models.ForeignKey(Apartment, null=True, blank=True)
    building = models.ForeignKey(Building, null=True, blank=True)
    payment_date = models.DateField(blank=True)
    # covered_date = models.DateField(blank=True)
    payment_type = models.CharField(max_length=300, null=True)
    payment_sum = models.DecimalField(decimal_places=2, max_digits=10, blank=True)
    currency = models.CharField(max_length=3, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    auto_id = models.CharField(max_length=36, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['apartment', '-payment_date']

    # def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
    #     if self.apartment is not None:
    #         self.apartment.fine = self.apartment.ongoing_fine()
    #         self.apartment.save()
    #     elif self.building is not None:
    #         self.building.fine = self.building.ongoing_fine()
    #         self.building.save()
    #     super(Payment, self).save(force_insert, force_update, using, update_fields)


class BillDetails(models.Model):
    added_by = models.ForeignKey(UserExtended, null=True, blank=True)
    name = models.CharField(max_length=300)
    bill = models.ForeignKey(Bill, null=True, blank=True)
    bill_sum = models.DecimalField(decimal_places=2, max_digits=10, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Compensation(models.Model):
    added_by = models.ForeignKey(UserExtended, null=True, blank=True)
    apartment = models.ForeignKey(Apartment, null=True, blank=True)
    date_from = models.DateField(blank=True)
    date_to = models.DateField(blank=True)
    credit_balance_on_day_of_compensation = models.DecimalField(max_digits=10, decimal_places=2, blank=True)
    compensation_approved_date = models.DateField(blank=True)
    compensation_no = models.CharField(max_length=50, blank=True)
    compensated_sum = models.DecimalField(max_digits=10, decimal_places=2, blank=True)
    compensated_credit = models.DecimalField(max_digits=10, decimal_places=2, blank=True)
    compensated_interest = models.DecimalField(max_digits=10, decimal_places=2, blank=True)

    def __str__(self):
        return self.compensation_no

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        schedules = []
        if len(self.apartment.loan_set.all()) > 0:
            complete_schedule = create_repayment_schedule(self.apartment.loan_set.all()[0])
            for loan in self.apartment.loan_set.all():
                schedules.append(create_repayment_schedule(loan))
            for i in range(len(schedules)):
                if i > 0:
                    j = 0
                    for dates, credit_balance, interest, sum_to_pay, loan_repayments, _, _, _, _ in schedules[i]:
                        complete_schedule[j][1] += credit_balance
                        complete_schedule[j][2] += interest
                        complete_schedule[j][3] += sum_to_pay
                        complete_schedule[j][4] += loan_repayments
                        j += 1
        self.compensated_sum = 0
        self.compensated_credit = 0
        self.compensated_interest = 0
        for schedule in complete_schedule:
            if self.date_from <= schedule[0] <= self.date_to:
                self.compensated_sum += schedule[3]
                self.compensated_credit += schedule[4]
                self.compensated_interest += schedule[2]
        self.credit_balance_on_day_of_compensation = self.apartment.outstanding_loans_balance(self.date_from)
        sums = calculate_compensation_sums(self.apartment, self.date_from, self.date_to)
        super(Compensation, self).save(force_insert, force_update, using, update_fields)
