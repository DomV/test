$(document).ready(function () {
	$('.building-form').validate({ 
        rules: {
            name: {
                required: true,
            },
            payment_date: {
            	required: true,
            },
            currency: {
                required: true,
            },
        },
        messages: {
        	name: { 
        		required: "Būtinas laukas",
        	},
        	area: { 
        		required: "Būtinas laukas",
        	},
            currency: { 
                required: "Būtinas laukas",
            },
        },
        submitHandler: function(form) {
        	$("#payment_sum").val($("#payment_sum").val().replace(",", "."))
            form.submit();
        }
	});
});