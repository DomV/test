$(document).ready(function () {
	$('.building-form').validate({ 
        rules: {
            bill_no: {
                required: true,
            },
            from_date: {
            	required: true,
            },
            to_date: {
                required: true,
            }
        },
        messages: {
        	bill_no: { 
        		required: "Būtinas laukas",
        	},
        	from_date: { 
        		required: "Būtinas laukas",
        	},
            to_date: { 
                required: "Būtinas laukas",
            },
        },
        submitHandler: function(form) {
            form.submit();
        }
	});
});