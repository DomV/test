$(document).ready(function () {
	$('.datepicker').datepicker({
		format: 'yyyy-mm-dd',
		language: 'lt',
		autoclose: true,
		forceParse: false,
        keyboardNavigation: false,

	});
	jQuery.validator.setDefaults({
	    highlight: function (element, errorClass, validClass) {
	        if (element.type === "radio") {
	            this.findByName(element.name).addClass(errorClass).removeClass(validClass);
	        } else {
	            $(element).closest('.form-group').removeClass('has-success has-feedback').addClass('has-error has-feedback');
	            $(element).closest('.form-group').find('i.fa').remove();
	            $(element).closest('.form-group').append('<i class="fa fa-exclamation fa-lg form-control-feedback"></i>');
	        }
	    },
	    unhighlight: function (element, errorClass, validClass) {
	        if (element.type === "radio") {
	            this.findByName(element.name).removeClass(errorClass).addClass(validClass);
	        } else {
	            $(element).closest('.form-group').removeClass('has-error has-feedback').addClass('has-success has-feedback');
	            $(element).closest('.form-group').find('i.fa').remove();
	            $(element).closest('.form-group').append('<i class="fa fa-check fa-lg form-control-feedback"></i>');
	        }
	    }
	});	
	$.validator.methods.range = function (value, element, param) {
	    var globalizedValue = value.replace(",", ".");
	    return this.optional(element) || (globalizedValue >= param[0] && globalizedValue <= param[1]);
	}
	 
	$.validator.methods.number = function (value, element) {
	    return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:[\s\.,]\d{3})+)(?:[\.,]\d+)?$/.test(value);
	}

	$(".clickable-row").click(function(e) {
        var senderElement = e.target;
        if (senderElement.tagName.toLowerCase() === 'td') {
            window.document.location = $(this).data("href");
        }
	});

//    $(".buttons").click(function() {
//        window.event.stopPropagation();
////       var event = e || window.event;
////        event.stopPropagation();
//    });

	$(".tablesorter").tablesorter();
    $('.selectpicker').selectpicker();

    $(".clear-parameters").click(function() {
        $("form input").val("");
    });
});	