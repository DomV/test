$(document).ready(function () {
	$('.building-form').validate({ 
        rules: {
            name: {
                required: true,
            },
        },
        messages: {
        	name: { 
        		required: "Būtinas laukas",
        	},
        	
        },
        submitHandler: function(form) {
        	$("#area").val($("#area").val().replace(",", "."))
            form.submit();
        }
	});

    $("#transfer-button").click(function() {
        $("#corr_street").val($("#reg_street").val());
        $("#corr_city").val($("#reg_city").val());
        $("#corr_post_code").val($("#reg_post_code").val());
        $("#corr_country").val($("#reg_country").val());
    });

});