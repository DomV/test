$(document).ready(function () {
	$('.building-form').validate({ 
        rules: {
            name: {
                required: true,
            },
            address: {
                required: true,
            },
            floor_count: {
            	required: true,
            	number: true,
            },
            apartment_count: {
            	required: true,
            	number: true,
            },
            area: {
            	required: true,
            	pattern: /^(\d+|\d+,\d{1,2})$/
            },
        },
        messages: {
        	name: { 
        		required: "Būtinas laukas",
        	},
        	address: { 
        		required: "Būtinas laukas",
        	},
        	floor_count: { 
        		required: "Būtinas laukas",
        		number: "Įveskite skaičių",
        	},
        	apartment_count: { 
        		required: "Būtinas laukas",
        		number: "Įveskite skaičių",
        	},
        	area: { 
        		required: "Būtinas laukas",
        		pattern: "Įveskite skaičių",
        	},
        },
        submitHandler: function(form) {
        	$("#area").val($("#area").val().replace(",", "."))
            form.submit();
        }
	});

    $('.new-building-button').click(function() {
        $("#area").val($("#area").val().replace(",", "."))
    });
});