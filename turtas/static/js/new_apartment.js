$(document).ready(function () {
	$('.building-form').validate({ 
        rules: {
            code: {
                required: true,
            },
            area: {
            	required: true,
            	number: true,
            },
        },
        messages: {
        	name: { 
        		required: "Būtinas laukas",
        	},
        	area: { 
        		required: "Būtinas laukas",
        		number: "Įveskite skaičių",
        	},
        },
        submitHandler: function(form) {
        	$("#area").val($("#area").val().replace(",", "."))
            form.submit();
        }
	});

	$('.new-building-button').click(function() {
		$("#area").val($("#area").val().replace(",", "."))
	});
});