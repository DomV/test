$(document).ready(function () {
	$('.apartment-form').validate({
        rules: {
            code: {
                required: true,
            },
            area: {
            	required: true,
            	number: true,
            },
        },
        messages: {
        	name: {
        		required: "Būtinas laukas",
        	},
        	area: {
        		required: "Būtinas laukas",
        		number: "Įveskite skaičių",
        	},
        },
        submitHandler: function(form) {
        	$("#area").val($("#area").val().replace(",", "."));
            form.submit();
        }
	});

    $('.payment-form').validate({
        rules: {
            code: {
                required: true,
            },
            area: {
            	required: true,
            	number: true,
            },
        },
        messages: {
        	name: {
        		required: "Būtinas laukas",
        	},
        	area: {
        		required: "Būtinas laukas",
        		number: "Įveskite skaičių",
        	},
        },
        submitHandler: function(form) {
        	$("#payment_sum").val($("#payment_sum").val().replace(",", "."));
            form.submit();
        }
	});

    jQuery(function($) {
        // consider adding an id to your table,
        // just incase a second table ever enters the picture..?
        var items = $("#payment-table tbody tr");

        var numItems = items.length;
        var perPage = 10;

        // only show the first 2 (or "first per_page") items initially
        items.slice(perPage).hide();

        // now setup your pagination
        // you need that .pagination-page div before/after your table
        $("#payment-pagination").pagination({
            items: numItems,
            itemsOnPage: perPage,
            prevText: "&laquo;",
            nextText: "&raquo;",
            onPageClick: function (pageNumber) { // this is where the magic happens
                // someone changed page, lets hide/show trs appropriately
                var showFrom = perPage * (pageNumber - 1);
                var showTo = showFrom + perPage;

                items.hide() // first hide everything, then show for the new page
                    .slice(showFrom, showTo).show();
            }
        });

        var checkFragment = function () {
            // if there's no hash, make sure we go to page 1
            var hash = window.location.hash || "#page-1";

            // we'll use regex to check the hash string
            hash = hash.match(/^#page-(\d+)$/);

            if (hash)
            // the selectPage function is described in the documentation
            // we've captured the page number in a regex group: (\d+)
                $("#pagination").pagination("selectPage", parseInt(hash[1]));
        };

        // we'll call this function whenever the back/forward is pressed
        $(window).bind("popstate", checkFragment);

        // and we'll also call it to check right now!
        checkFragment();
    });

    $('#accrued_interest').change(function() {
        if ($(this).is(':checked')) {
            $('#contract_no').val('Atidėtos palūkanos');
            $('#interest_rate').val('0,00')
        } else {
            $('#contract_no').val('DNKS');
            $('#interest_rate').val('3,00')
        }
    });

	$('.loan-form').validate({
        rules: {
            contract_no: {
                required: true,
            },
            loan_sum: {
                required: true,
                number: true,
            },
            loan_type: {
                required: true,
            },
            interest_rate: {
                required: true,
                number: true,
            },
            valuta: {
                required: true,
            },
            calculate_from_outstanding_shares: {
                required: true,
            },
            payment_day: {
                required: true,
                number: true,
            },
            days_interest_is_payable: {
                required: true,
                number: true,
            },
            valid_from: {
                required: true,
            },
            valid_to: {
                required: true,
            },
            calculate_fine: {
                required: true,
            },
        },
        messages: {
        	contract_no: {
                required: "Būtinas laukas",
            },
            loan_sum: {
                required: "Būtinas laukas",
                pattern: "Įveskite skaičių",
            },
            loan_type: {
                required: "Būtinas laukas",
            },
            interest_rate: {
                required: "Būtinas laukas",
                pattern: "Įveskite skaičių",
            },
            valuta: {
                required: "Būtinas laukas",
            },
            calculate_from_outstanding_shares: {
                required: "Būtinas laukas",
            },
            payment_day: {
                required: "Būtinas laukas",
                pattern: "Įveskite skaičių",
            },
            days_interest_is_payable: {
                required: "Būtinas laukas",
                pattern: "Įveskite skaičių",
            },
            valid_from: {
                required: "Būtinas laukas",
            },
            valid_to: {
                required: "Būtinas laukas",
            },
            calculate_fine: {
                required: "Būtinas laukas",
            },

        },
        submitHandler: function(form) {
        	$("#loan_su2m").val($("#loan_sum2").val().replace(",", "."));
        	$("#interest_rate").val($("#interest_rate").val().replace(",", "."));
        	$("#monthly_payment").val($("#monthly_payment").val().replace(",", "."));
        	$("#outstanding_loan_balance").val($("#outstanding_loan_balance").val().replace(",", "."));
            form.submit();
        }
	});

});
