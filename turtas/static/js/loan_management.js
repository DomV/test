/**
 * Created by Dominykas on 11/01/16.
 */

$(document).ready(function() {
    $("#add-selects").click(function() {
        var html = $("#loan-select").wrap('<p/>').parent().html();
        var new_el = $(html);
        $(new_el).insertBefore("#add-selects");
        (new_el).find(".bootstrap-select").remove();
        $(new_el).find(".selectpicker").selectpicker();
    });


    $('#loan-join-form').validate({
        rules: {
            apartment: {
                required: true,
            },
            from_date: {
            	required: true,
            },
            to_date: {
                required: true,
            }
        },
        apartment: {
        	loan: {
        		required: "Būtinas laukas",
        	},
        	from_date: {
        		required: "Būtinas laukas",
        	},
            to_date: {
                required: "Būtinas laukas",
            },
        },
        submitHandler: function(form) {
            form.submit();
        }
	});
});