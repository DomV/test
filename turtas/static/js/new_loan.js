$(document).ready(function () {

    $('#accrued_interest').change(function() {
        if ($(this).is(':checked')) {
            if (!$('#contract_no').val()) {
                $('#contract_no').val('Atidėtos palūkanos');
            }
            $('#interest_rate').val('0,00')
        } else {
            if ($('#contract_no').val().length == 0) {
                $('#contract_no').val('DNKS');
            }
            $('#interest_rate').val('3,00')
        }
    });

	$('.building-form').validate({ 
        rules: {
            contract_no: {
                required: true,
            },
            loan_sum: {
                required: true,
                number: true,
            },
            loan_type: {
                required: true,
            },
            interest_rate: {
                required: true,
                number: true,
            },
            valuta: {
                required: true,
            },
            calculate_from_outstanding_shares: {
                required: true,
            },

            days_interest_is_payable: {
                required: true,
                number: true,
            },
            valid_from: {
                required: true,
            },
            valid_to: {
                required: true,
            },
            calculate_fine: {
                required: true,
            },
        },
        messages: {
        	contract_no: {
                required: "Būtinas laukas",
            },
            loan_sum: {
                required: "Būtinas laukas",
                pattern: "Įveskite skaičių",
            },
            loan_type: {
                required: "Būtinas laukas",
            },
            interest_rate: {
                required: "Būtinas laukas",
                pattern: "Įveskite skaičių",
            },
            valuta: {
                required: "Būtinas laukas",
            },
            calculate_from_outstanding_shares: {
                required: "Būtinas laukas",
            },
            payment_day: {
                required: "Būtinas laukas",
                pattern: "Įveskite skaičių",
            },
            days_interest_is_payable: {
                required: "Būtinas laukas",
                pattern: "Įveskite skaičių",
            },
            valid_from: {
                required: "Būtinas laukas",
            },
            valid_to: {
                required: "Būtinas laukas",
            },
            calculate_fine: {
                required: "Būtinas laukas",
            },
        	
        },
        submitHandler: function(form) {
        	$("#loan_sum2").val($("#loan_sum2").val().replace(",", "."));
        	$("#interest_rate").val($("#interest_rate").val().replace(",", "."));
        	$("#monthly_payment").val($("#monthly_payment").val().replace(",", "."));
        	$("#outstanding_loan_balance").val($("#outstanding_loan_balance").val().replace(",", "."));
            form.submit();
        }
	});
});