$(document).ready(function () {
	$('.bill-form').validate({ 
        rules: {
            bill_no: {
                required: true,
            },
            from_date: {
            	required: true,
            },
            to_date: {
                required: true,
            }
        },
        messages: {
        	bill_no: { 
        		required: "Būtinas laukas",
        	},
        	from_date: { 
        		required: "Būtinas laukas",
        	},
            to_date: { 
                required: "Būtinas laukas",
            },
        },
        submitHandler: function(form) {
            form.submit();
        }
	});

    $('.payment-form').validate({ 
        rules: {
            name: {
                required: true,
            },
            payment_date: {
                required: true,
            },
            currency: {
                required: true,
            }
        },
        messages: {
            name: { 
                required: "Būtinas laukas",
            },
            area: { 
                required: "Būtinas laukas",
            },
            currency: { 
                required: "Būtinas laukas",
            },
        },
        submitHandler: function(form) {
            $("#payment_sum").val($("#payment_sum").val().replace(",", "."));
            form.submit();
        }
    });

    $('.compensation-form').validate({

        submitHandler: function(form) {
            form.submit();
        }
    });

    jQuery(function($) {
        // consider adding an id to your table,
        // just incase a second table ever enters the picture..?
        var items = $("#payment-table tbody tr");

        var numItems = items.length;
        var perPage = 10;

        // only show the first 2 (or "first per_page") items initially
        items.slice(perPage).hide();

        // now setup your pagination
        // you need that .pagination-page div before/after your table
        $("#payment-pagination").pagination({
            items: numItems,
            itemsOnPage: perPage,
            prevText: "&laquo;",
            nextText: "&raquo;",
            onPageClick: function (pageNumber) { // this is where the magic happens
                // someone changed page, lets hide/show trs appropriately
                var showFrom = perPage * (pageNumber - 1);
                var showTo = showFrom + perPage;

                items.hide() // first hide everything, then show for the new page
                    .slice(showFrom, showTo).show();
            }
        });

        var checkFragment = function () {
            // if there's no hash, make sure we go to page 1
            var hash = window.location.hash || "#page-1";

            // we'll use regex to check the hash string
            hash = hash.match(/^#page-(\d+)$/);

            if (hash)
            // the selectPage function is described in the documentation
            // we've captured the page number in a regex group: (\d+)
                $("#payment-pagination").pagination("selectPage", parseInt(hash[1]));
        };

        // we'll call this function whenever the back/forward is pressed
        $(window).bind("popstate", checkFragment);

        // and we'll also call it to check right now!
        checkFragment();
    });

//    jQuery(function($) {
//        // consider adding an id to your table,
//        // just incase a second table ever enters the picture..?
//        var items = $(".repayment-table tbody tr");
//
//        var numItems = items.length;
//        var perPage = 10;
//
//        // only show the first 2 (or "first per_page") items initially
//        items.slice(perPage).hide();
//
//        // now setup your pagination
//        // you need that .pagination-page div before/after your table
//        $("#repayment-pagination").pagination({
//            items: numItems,
//            itemsOnPage: perPage,
//            prevText: "&laquo;",
//            nextText: "&raquo;",
//            onPageClick: function (pageNumber) { // this is where the magic happens
//                // someone changed page, lets hide/show trs appropriately
//                var showFrom = perPage * (pageNumber - 1);
//                var showTo = showFrom + perPage;
//
//                items.hide() // first hide everything, then show for the new page
//                    .slice(showFrom, showTo).show();
//            }
//        });
//
//        var checkFragment = function () {
//            // if there's no hash, make sure we go to page 1
//            var hash = window.location.hash || "#rp-page-1";
//
//            // we'll use regex to check the hash string
//            hash = hash.match(/^#rp-page-(\d+)$/);
//
//            if (hash)
//            // the selectPage function is described in the documentation
//            // we've captured the page number in a regex group: (\d+)
//                $("#repayment-pagination").pagination("selectPage", parseInt(hash[1]));
//        };
//
//        // we'll call this function whenever the back/forward is pressed
//        $(window).bind("popstate", checkFragment);
//
//        // and we'll also call it to check right now!
//        checkFragment();
//    });
});