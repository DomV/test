from django.conf.urls import patterns, url
from django.contrib.auth.views import logout
from django.contrib.staticfiles.views import serve as serve_static
from django.views.decorators.cache import never_cache

from .views import (BuildingList, BuildingDetail, BuildingCreate, BuildingUpdate, BuildingDelete, BuildingBalance,
                    ClientList, ClientDetail, ClientCreate, ClientUpdate, ClientDelete, ApartmentList, ApartmentDetail,
                    ApartmentCreate, ApartmentUpdate, ApartmentDelete, LoanList, LoanDetail, LoanCreate, LoanUpdate,
                    LoanDelete, LoanSplit, BillList, BillDetail, BillCreate, BillUpdate, BillDelete, PaymentList,
                    PaymentDetail, PaymentCreate, PaymentUpdate, PaymentDelete, BillView, RepaymentSchedule,
                    RepaymentScheduleCombined, FineView, FineViewSB, ResetFine, CompensationCreate, CompensationView,
                    CompensationDelete, DocumentsView, LoanManagementView, ProfileView, CompensationUpdate)

urlpatterns = patterns('turtas.views',
    url(r'^login/$', 'login_user', name='login_user'),
    url(r'^logout/$', logout, {'next_page': '/login/'}, name='logout'),
    url(r'^$', 'home', name='home'),
    url(r'^buildings/$', BuildingList.as_view(), name='building_list'),
    url(r'^buildings/(?P<pk>[0-9]+)/$', BuildingDetail.as_view(), name='building_detail'),
    url(r'^buildings/new/$', BuildingCreate.as_view(), name='new_building'),
    url(r'^buildings/update/(?P<pk>[0-9]+)$', BuildingUpdate.as_view(), name='update_building'),
    url(r'^buildings/delete/(?P<pk>[0-9]+)$', BuildingDelete.as_view(), name='delete_building'),
    url(r'^buildings/balance/(?P<pk>[0-9]+)/$', BuildingBalance.as_view(), name='building_balance'),
    url(r'^clients/$', ClientList.as_view(), name='client_list'),
    url(r'^clients/(?P<pk>[0-9]+)/$', ClientDetail.as_view(), name='client_detail'),
    url(r'^clients/new/$', ClientCreate.as_view(), name='new_client'),
    url(r'^clients/update/(?P<pk>[0-9]+)$', ClientUpdate.as_view(), name='update_client'),
    url(r'^clients/delete/(?P<pk>[0-9]+)$', ClientDelete.as_view(), name='delete_client'),
    url(r'^apartments/$', ApartmentList.as_view(), name='apartment_list'),
    url(r'^apartments/(?P<pk>[0-9]+)/$', ApartmentDetail.as_view(), name='apartment_detail'),
    url(r'^apartments/new/$', ApartmentCreate.as_view(), name='new_apartment'),
    url(r'^apartments/update/(?P<pk>[0-9]+)$', ApartmentUpdate.as_view(), name='update_apartment'),
    url(r'^apartments/delete/(?P<pk>[0-9]+)$', ApartmentDelete.as_view(), name='delete_apartment'),
    url(r'^loans/$', LoanList.as_view(), name='loan_list'),
    url(r'^loans/(?P<pk>[0-9]+)/$', LoanDetail.as_view(), name='loan_detail'),
    url(r'^loans/new/$', LoanCreate.as_view(), name='new_loan'),
    url(r'^loans/update/(?P<pk>[0-9]+)$', LoanUpdate.as_view(), name='update_loan'),
    url(r'^loans/delete/(?P<pk>[0-9]+)$', LoanDelete.as_view(), name='delete_loan'),
    url(r'^loans/split/(?P<pk>[0-9]+)$', LoanSplit.as_view(), name='split_loan'),
    url(r'^loans/schedule/(?P<pk>[0-9]+)/$', RepaymentSchedule.as_view(), name='repayment_schedule'),
    url(r'^loans/schedule/(?P<type>[0-9]+)/(?P<pk>[0-9]+)/$', RepaymentScheduleCombined.as_view(),
        name='repayment_schedule_combined'),
    url(r'^bills/$', BillList.as_view(), name='bill_list'),
    url(r'^bills/(?P<pk>[0-9]+)/$', BillDetail.as_view(), name='bill_detail'),
    url(r'^bills/new/$', BillCreate.as_view(), name='new_bill'),
    url(r'^bills/update/(?P<pk>[0-9]+)$', BillUpdate.as_view(), name='update_bill'),
    url(r'^bills/delete/(?P<pk>[0-9]+)$', BillDelete.as_view(), name='delete_bill'),
    url(r'^payments/$', PaymentList.as_view(), name='payment_list'),
    url(r'^payments/(?P<pk>[0-9]+)/$', PaymentDetail.as_view(), name='payment_detail'),
    url(r'^payments/new/$', PaymentCreate.as_view(), name='new_payment'),
    url(r'^payments/update/(?P<pk>[0-9]+)$', PaymentUpdate.as_view(), name='update_payment'),
    url(r'^payments/delete/(?P<pk>[0-9]+)$', PaymentDelete.as_view(), name='delete_payment'),
    url(r'^bills/(?P<pk>[0-9]+)/generated/$', BillView.as_view(), name='bill_view'),
    url(r'^fines/$', FineView.as_view(), name='fines'),
    url(r'^fines-sb/$', FineViewSB.as_view(), name='fines_sb'),
    url(r'^reset-fine/(?P<pk>[0-9]+)$', ResetFine.as_view(), name='reset_fine'),
    url(r'^new_compensation/$', CompensationCreate.as_view(), name='new_compensation'),
    url(r'^compensations/$', CompensationView.as_view(), name='compensation_view'),
    url(r'^compensations/update/(?P<pk>[0-9]+)$', CompensationUpdate.as_view(), name='update_compensation'),
    url(r'^compensations/delete/(?P<pk>[0-9]+)$', CompensationDelete.as_view(), name='delete_compensation'),
    url(r'^documents/$', DocumentsView.as_view(), name='documents_view'),
    url(r'^loan-management/$', LoanManagementView.as_view(), name='loan_management'),
    url(r'^profile/(?P<pk>[0-9]+)$', ProfileView.as_view(), name='profile_view'),

    url(r'^static/(?P<path>.*)$', never_cache(serve_static)),
)