from django_filters import FilterSet, CharFilter

from .models import Building, Payment, Apartment, Loan, Client


class BuildingFilter(FilterSet):
    # name = CharFilter(lookup_type='icontains')

    class Meta:
        model = Building
        fields = {'name': ['icontains']}


class ClientFilter(FilterSet):

    class Meta:
        model = Client
        fields = {'name': ['icontains']}


class PaymentFilter(FilterSet):

    class Meta:
        model = Payment
        fields = {'apartment__building__name': ['icontains'],
                  'apartment__code': ['icontains'],
                  'payment_date': ['gt', 'lt']}


class ApartmentFilter(FilterSet):

    class Meta:
        model = Apartment
        fields = {'building__name': ['icontains']}


class LoanFilter(FilterSet):

    class Meta:
        model = Loan
        fields = {'apartment__building__name': ['icontains']}